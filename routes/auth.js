/*****************************************************************
* Module Name: Authentication router
* Description: Authentication router of Ahitis server
* Author: Brian Lai
* Date Modified: 2016.01.04
* Related files: n/a
*****************************************************************/
//This router is for check if the access token stored in client device generated at previous login is valid or not.
//To be easy, it's for auto login once the user login before. Not the same purpose in the app.js middleware.
exports.auth = function(req,res){
	//Grab object send by client device.
	var accessToken = req.body.accessToken;
	var deviceUUID = req.body.deviceUUID;
	//Prepare response object.
	var result = {
			result : ""
		};
	
	//If no deviceUUID, it should be from the web and should be force to login process.
	if(!deviceUUID){
		deviceUUID = "web";
		result.result = "Access token expired";
		res.status(419).json(result);
		return;
	}

	console.log("deviceID " + deviceUUID + " trying to get Authentication...");

	//Get the access token object from DB using the deviceUUID. (With the deviceUUID, it can prevent some hacker to hack the access token from other machine.)
	req.db.AccessToken.findOne({deviceUUID: deviceUUID, accessToken: accessToken}, function(err, at){
		if(err){ //DB error
			result.result = "Check key error.";
			res.status(500).json(result);
			return;
		}
		
		//7 day expire
		var expire = new Date();
		expire.setDate(expire.getDate() - 7);
		
		//Access token entry found
		if(at){
			if (Math.floor(expire - at.createdDate) <= 0) { //not expired
				result.result = "Access token valid.";
				at.createdDate = new Date(); //Update the access token to current date.
				at.save(function(err, product, numAff) {
					if (err) {
						console.log("Update key error");
						result.result = "Update key error.";
						res.status(500).json(result);
						return;
					}
					res.status(200).json(result);
					return;
				});
			} else { //expired
				result.result = "Access token expired.";
				res.status(419).json(result);
				return;
			}
		}else{ //No access Token entry found.
			result.result = "Invalid Access token.";
			res.status(401).json(result);
			return;
		}
	});
};