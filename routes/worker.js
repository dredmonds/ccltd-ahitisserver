/*******************************************************************************
 * Module Name: worker information router.
 * Description: worker information router of Ahitis server. (This router is used for all worker related process.)
 * Author: Brian Lai Date Modified: 2016.01.05.
 * Related files: n/a.
 ******************************************************************************/
var https = require('https');
var notification = require('../routes/notification');

//This route is used for toggle worker to online status.
exports.onlineWorker = function(req, res) {
	//Grab object send by client device.
	var loginID = req.loginID;
	var long = req.body.long;
	var lat = req.body.lat;
	var skillID;
	var countryCode = "";
	//Prepare the response object.
	var result = {
		result : ""
	};
	//for Debug use: (This is CC HK office GEO location)
	//long = "114.1485286";
	//lat = "22.3366098";

	//console.log("Worker "+ loginID + " trying to online: ("+long+","+lat+")");
	req.db.OnlineWorker.findOne({loginID:loginID}, function(err,worker){
		if(err){ //DB Error
			result.result = "DB error.";
			res.status(500).json(result);
			return;
		}
		if(worker){
			//existingOnlineWorker = true;
			//console.log(loginID + " Already online. Updating location.");
			worker.loc = [long, lat];


			worker.save(function(err, product, numAff) {
				if (err) {
					console.log("Update worker location error");
					result.result = "Update worker location error.";
					res.status(500).json(result);
					return;
				}

				//Check if the worker arrive the job location if it assigned a job already and update the job start datetime if needed
				if(worker.jobID){
					req.db.Job.findOne({jobID: worker.jobID, loc:{$near: {$geometry:{type: "Point", coordinates: [long,lat]}, $maxDistance: 50}}}, function(err,job){
						if(err){
							console.log("Finding job start time error");
							result.result = "Finding job start time error.";
							res.status(500).json(result);
							return;
						}

						if(job){
							job.createdDate = new Date();
							job.updatedDate = new Date();
							job.updatedBy = worker.loginID;

							job.save(function(err, product, numAff) {
								if(err){
									console.log("Update job start time error");
									result.result = "Update job start time error.";
									res.status(500).json(result);
									return;
								}
							});
						}
					})
				}


				res.status(200).json(result);
				return;
			});
		}else{
			req.db.User.findOne({loginID: loginID}, function(err, logID) {
				if(err){ //DB Error
					result.result = "DB error.";
					res.status(500).json(result);
					return;
				}

				console.log(loginID + " going to online");


				if(logID){
					if(logID.loginID === "") {
						console.log("worker not exists");
						result.result = "Worker not exists.";
						res.status(403).json(result);
						return;
					} else {
						skillID = logID.skill.skillID;
					}
				}
			});


			//this option object is for Google API use. (Pass the coordinate to get the address)
			var optionsget= {
					host: 'maps.googleapis.com',
					port: 443,
					path: '/maps/api/geocode/json?latlng=' + lat +',' + long + '&result_type=country&key=' + req.googleKey,
					method: 'GET'
			};

			console.info('Calling Google Map GeoLocation API for country code');
			// do the google GET request
			var location = "";
			https.get(optionsget, function(googleRes) {
			    console.log("statusCode: ", googleRes.statusCode);

			    //The http get request handle the response from google in stream mode.
			    //Therefore, the on 'data' event may not contains all the data. Google may send the information in multiple packet.
			    //the on 'data' event here to append the characters into location variable.
			    //The on 'end' event handle the location variable to obtain the country code.
			    googleRes.on('data', function(d) {
			    	location += d.toString('utf8');
			    }).on('end', function(){
					var locationObj = JSON.parse(location); //Parse the string into JSON object.
			    	if(locationObj.status === "OK"){ //If status from google say OK
			    		for(var i in locationObj.results){ //loop through the results
			    			if(locationObj.results[i].types[0] === "country"){ //Checks if the location type is country.
			    				countryCode = locationObj.results[i].address_components[0].short_name; //get the short_name from address_commponents. This is the ISO country Code.
			    			}
			    		}
			    	}

			    	//Prepare the onlineWorker DB object
			    	var onlineWorker = new req.db.OnlineWorker({
			    		loginID : loginID,
			    		skillID: skillID,
			    		loc: [long, lat],
			    		countryCode: countryCode,
			    		jobID: null,
			    		onlineDateTime : new Date()
			    	});

			    		//Save the online worker entry to DB.
			    		onlineWorker.save(function(err, product, numAff) {
			    			if (err) { //DB error
			    				console.log("Error online worker " + loginID);
									console.log(JSON.stringify(err));
			    				result.result = "onlineWorker error.";
			    				res.status(500).json(result);
			    				return;
			    			}
			    			//	Success
			    			//	notification.send(req, loginID, "You're online now.");
			    			result.result = "Worker " + loginID + " online.";
			    			res.status(200).json(result);
								return;
			    		});
					});
			}).on('error', function(e) {
				console.error(e);
				result.result = "Get countryCode error.";
				res.status(500).json(result);
				return;
			});
		}
	});

};

//This route is used for toggle worker to offline status.
exports.offlineWorker = function(req, res) {
	//Get the loginID obtained in the middleware using the access token.
	var loginID = req.loginID;
	var result = {
			result : ""
		};

		req.db.OnlineWorker.findOne({loginID: loginID}, function(err,worker){
			if(err){ //DB error
				result.result = "Offline Worker error.";
				res.status(500).json(result);
				return;
			}

			if(worker){
				if(worker.jobID == null || worker.jobID.length == 0){
					//Remove the onlineworker from DB using the loginID.
					req.db.OnlineWorker.remove({loginID: loginID}, function(err){
						if(err){ //DB error
							result.result = "Offline Worker error.";
							res.status(500).json(result);
							return;
						}
						//Success
						//notification.send(req, loginID, "You're offline now.");
						result.result = "Worker " + loginID + " offline.";
						res.status(200).json(result);
					});
				}else{
					result.result = "Cannot offline, You are currently working on a job.";
					res.status(406).json(result);
					return;
				}
			}else{
				console.log("Worker is already offline.");
				result.result = "Worker " + loginID + " offline.";
				res.status(200).json(result);
			}
		});
};

exports.getWorker = function(req, res){
	var workerID = req.body.workerID;
	//Prepare for response object
	var result = {
		result: "",
		loginID: "",
		password: "",                     /*insert this to re-used for updating worker profile*/
	    firstname: "",
	    lastname: "",
	    email: "",
	    userType: "",
	    countryCode: "",
	    skillID: "",
	    ranking: "",
	    createdDate: ""
	};
	//DB query to worker to get user's information
	req.db.User.findOne({loginID: workerID}, function(err, logID) {
		if(err){ //DB Error
			result.result = "DB error.";
			res.status(500).json(result);
			return;
		}

		if(logID){
			if(logID.loginID === "") {
				result.result = "Worker's profile DB query are empty.";
				res.status(403).json(result);
				return;
			} else {
					result.loginID = logID.loginID;
					result.password = logID.password;           /*insert this to re-used for updating worker profile*/
					result.firstname = logID.firstname;
					result.lastname = logID.lastname;
					result.email = logID.email;
					result.contactNo = logID.contactNo;
					result.userType = logID.userType;
					result.countryCode = logID.countryCode;
					result.skillID = logID.skill.skillID;
					result.ranking = logID.skill.ranking;
					result.duration = logID.skill.duration;
					result.amount = logID.skill.amount;
					result.profilePict = logID.profilePict;
					result.createdDate = logID.createdDate;
					console.log("worker's profile DB query success.");
					res.status(200).json(result);
					return;
			 }
			}
			else{
				console.log("No worker associated.");
				res.status(200).json(result);
				return;
			}
		});

}


//Get worker's profile information
exports.profile = function(req, res) {    /*inserted by dredmonds 2016.01.19*/
	//Prepare for response object
	var result = {
		result: "",
		skill: {}
	};
	//DB query to worker to get user's information
	req.db.User.findOne({loginID: req.loginID}, function(err, logID) {
		if(err){ //DB Error
			result.result = "DB error.";
			res.status(500).json(result);
			return;
		}

		if(logID){
			if(logID.loginID === "") {
				result.result = "Worker's profile DB query are empty.";
				res.status(403).json(result);
				return;
			} else {
					result.loginID = logID.loginID;
					result.password = logID.password;           /*insert this to re-used for updating worker profile*/
					result.firstname = logID.firstname;
					result.lastname = logID.lastname;
					result.email = logID.email;
					result.contactNo = logID.contactNo;
					result.userType = logID.userType;
					result.countryCode = logID.countryCode;
					result.skillID = logID.skill.skillID;
					result.ranking = logID.skill.ranking;
					result.duration = logID.skill.duration;
					result.amount = logID.skill.amount;
					result.activationStatus = logID.activationStatus;
					result.accountNumber = logID.card.accountNumber;
					result.createdDate = logID.createdDate;
					result.profilePict = logID.profilePict;
					console.log("worker's profile DB query success.");
					res.status(200).json(result);
					return;
			}
		}else{
			result.result = "Worker's profile DB query are empty.";
			res.status(403).json(result);
			return;
		}
	});

}; /*End of Get worker's profile information*/


//Update worker's profile information
exports.update = function(req, res) {             /*inserted by dredmonds 2016.01.19*/
	//Grab object send by client device.
	var username = req.body.username;             /*No rules yet, if included for updates*/
	var password = req.body.password;
	var firstname = req.body.firstname;
	var lastname = req.body.lastname;
	var email = req.body.email;
	var contactNo = req.body.contactNo;
	//var userType = req.body.userType;           /*No rules yet, if included for updates*/
	var countryCode = req.body.countryCode;
	var skillID = req.body.skillID;
	var profilePict = req.body.profilePict;  /*inserted to update worker's profile picture*/

	console.log(profilePict);
	//Prepare the response object.
	var result = {
		result : ""
	};

	console.log("Worker " + username + " trying to updates information...");

	//Prepare the DB object
	var updtWorker = {
			//logID: username,                   /*No rules yet, if included for updates*/
			password : password,
			firstname : firstname,
			lastname : lastname,
			email : email,
			contactNo : contactNo,
			//userType: userType,                /*No rules yet, if included for updates*/
			countryCode: countryCode,
			skill: {
				skillID: skillID
			},
			updatedBy: req.logID,
			updatedDate: new Date(),
			profilePict: profilePict             /*inserted to update worker's profile picture*/
			};

	//Update User DB
	req.db.User.update({loginID: req.loginID},{$set: updtWorker}, {multi:true}, function(err, product, numAff) {
		if (err) {//DB Error
			console.log("Error updating user " + username);
			console.log(err.message);
			result.result = "Update error.";
			res.status(500).json(result);
			return;
		}
		//Success
		result.result = "Worker " + username + " updated.";
		res.status(200).json(result);
	});/*end of DB update*/

};  /*End of Update worker's profile information*/


//Authenticate worker password for w_profile_edit
exports.authPassword = function(req, res){             /*inserted by dredmonds 2016.02.01*/
	//Get the loginID obtained in the middleware using the access token.
	var loginID = req.loginID;
	//Grab object send by client device.
	var password = req.body.password;
	var result = {
			result : ""
		};

	console.log("User password " + loginID + " trying to verify...");

	//Search for the user in Database
	req.db.User.findOne({loginID: loginID}, function(err, user){
		if(err){ //DB error
			result.result = "500";
			res.status(500).json(result);
			return;
		}

		if(user){
			if(user.password === password){
				result.result = "200";
				res.status(200).json(result);
				return;
			}else{ //Password not match.
				result.result = "403";
				res.status(403).json(result);
				return;
			}

		}else{ //User not found
			result.result = "403";
			res.status(403).json(result);
			return;
		}

	});
};

exports.jobReject = function(req,res){
	var loginID = req.loginID;
	var jobID = req.body.jobID;
	var result = {
			result : "",
		};

	console.log("Reject job for worker: " + jobID);

	//Search onlineworker
/*
	req.db.OnlineWorker.findOne({loginID: loginID, jobID: jobID}, function(err, worker){
		if(err){
			result.result = "Search Online Worker error";
			res.status(500).json(result);
			return;
		}

		if(worker){
			worker.jobID = null;
			worker.save(function(err, product, numAff) {
	    		if (err) { //DB error
	    			console.log("Error reject JobID in online worker: " + worker.loginID);
	    			result.result = "reject Online Worker error.";
	    			res.status(500).json(result);
	    			return;
	    		}

				result.result = "Rejected";
				res.status(200).json(result);
				return;
			});
		}else{
			result.result = "No worker found.";
			res.status(204).json(result);
			return;
		}
	});
	*/
	result.result = "Rejected";
	res.status(200).json(result);
	return;
};

//This route is for search worker
exports.search = function(req, res){
	var loginID = req.loginID;
	var skillID = req.body.skillID;
	var long = req.body.location.long;
	var lat = req.body.location.lat;
	var duration = req.body.duration;
	var countryCode = req.body.countryCode;
	var result = {
		result : "",
		jobID: ""
	};

	console.log("Searching worker for: " + loginID);

	//Search worker
	req.db.OnlineWorker.find({jobID: null, loc:{$near: {$geometry:{type: "Point", coordinates: [long,lat]}, $maxDistance: 16000}}}, function(err, workers){
		if(err){
			result.result = "500";
			res.status(500).json(result);
			return;
		}

		if(workers && workers.length > 0){

			req.db.Skill.findOne({skillid: parseInt(skillID), ranking: 0}, function(errSkill,skill){
				if(errSkill){
					result.result = "500";
					res.status(500).json(result);
					return;
				}

				if(skill){
					var skillName = skill.name;

					var job = new req.db.Job({
						userID: loginID,
						workerID: "",
						skillID: skillID,
						loc: [long,lat],
						countryCode: countryCode,
						statusID: 1,
						paymentStatusID: 1,
						createdBy: "SYSTEM",
						createdDate: new Date(),
						updatedBy: "SYSTEM",
						updatedDate: new Date()
					});

					job.save(function(err, product, numAff) {
			    		if (err) { //DB error
			    			console.log("Error create job session for user: " + loginID);
			    			result.result = "Create job error.";
			    			res.status(500).json(result);
			    			return;
			    		}

			    		workers.forEach(function(worker){
			    	    		try{
			    	    			notification.send(req, worker.loginID, "Job Arrive: ("+duration+" Hours) An Ahitis user seeking for "+skillName.toUpperCase()+" help. Please reply in 30 second.", product.jobID.toHexString());
			    	    		}catch(err){
			    	    			console.log("To Worker: " + worker.loginID + "Notification send error.");
			    	    		}
			    		});

						result.result = "200";
						result.jobID = product.jobID.toHexString();
						result.workers = workers.length;
						res.status(200).json(result);
						return;
			    });
				}
			});

		}else{ //No worker found
			console.log("No worker found");
			result.result = "No worker found.";
			res.status(204).json(result);
			return;
		}
	});
};
