/*****************************************************************
* Module Name: Payment information router
* Description: Payment information router of Ahitis server
* Author: dredmonds
* Date Modified: 2016.02.03
* Related files: n/a
*****************************************************************/
var https = require('https');
var querystring = require('querystring');
var fs = require('fs');


exports.getToken = function(req, res){
  //Generates payment token here...
  var result = {
  	result: "",
  	paymentToken: "",
  };
  
  req.gateway.clientToken.generate({}, function(err, response) {
  	if(err){ //Error generating token
  	   result.result = "Error";
  	   res.status(500).json(result);
  	   return;
  	}
  	
  	if(response){
  	   result.result = "Ok";
  	   result.paymentToken = response.clientToken;
  	   res.status(200).json(result);
  	}
  });

};


exports.postTransaction = function(req, res){
  var nonceFromTheClient = req.body.payment_method_nonce || req.query.payment_method_nonce;
  var amount = req.body.amount || req.query.amount;
  var orderID = req.body.custom || req.query.custome;
  var currency = req.body.currency || req.currency;
  var merchantAccount;

  /*CCLtd Merchant Accounts(Currency) set on Braintree Account*/
  switch(currency){
    case "GBP":
      merchantAccount = "cc-ltd";
      break;
    case "USD":
      merchantAccount = "ccltd-USD";
      break;
    case "HKD":
      merchantAccount = "ccltd-HKD";
      break;
    case "EUR":
      merchantAccount = "ccltd";
      break;	  
    default:
      break;
  }  
  
  /*Create Sale Transaction*/
  req.gateway.transaction.sale({
	  amount: amount,
	  merchantAccountId: merchantAccount,
	  paymentMethodNonce: nonceFromTheClient,
	  orderId: orderID,
	  options: {
	    submitForSettlement: true
	  }
    }, function (err, result) {
  	  
      if(err){
        var tXnResultsErr = new req.db.Transaction({
        	transactionID: 'Error',
        	jobID: orderID,
        	amount: amount,
        	currency: merchantAccount,
        	paymentDate: new Date(),
        	paymentStatus: 'TXn Error',
        	createdBy: 'Braintree_v.zero',
            createdDate: new Date(),
            updatedBy: 'Braintree',
            updatedDate: new Date()
        });

        //Save to Transaction collections.
        tXnResultsErr.save(function(err, product, numAff){
          if (err){
            console.log("Data error on Job ID: " + orderID);
          }else{
          	console.log("Data error saved on Job ID: " + orderID);
          }//End if condition
		  
          console.log("Braintree Sale Transaction Error: " + err);
          //Payment Transaction Error Details Page
          res.send(sendTransactionError("Error", orderID, "0.00")); //inserted by dredmonds 2016.09.12
          return;
		  
        });//End txnPaymentResult.save
      }//End if-condition (err)
	  

	  
	  if(result.success) {
		var transaction = result.transaction;
        var tXnResultsSuc = new req.db.Transaction({
          transactionID: transaction.id,
          jobID: transaction.orderId,
          amount: transaction.amount,
          currency: transaction.currencyIsoCode,
          paymentDate: transaction.createdAt,
          paymentStatus: transaction.status,
          createdBy: 'Braintree_v.zero',
          createdDate: new Date(),
          updatedBy: 'Braintree',
          updatedDate: new Date()
        });
         
        console.log("going to save transaction"); //Save to Transaction collections.
        
        tXnResultsSuc.save(function(err, product, numAff){
          if (err){
            console.log("DB error on Transaction Txn ID: " + transaction.id);
          }else{
            console.log("DB success on Transaction Txn ID: " + transaction.id);
          }//End if-else condition (err)
        });//End txnPaymentResult.save
		

            console.log("Going to find job in db: " + transaction.orderId); //Find job information in DB.
            
            req.db.Job.findOne({jobID: orderID.toString()}, function(err, job){
              if(err){
                console.log("Can't find Job ID, error on creating draft invoice. " + err);
              }//End if-condition (err)
              
              if(job){
                var pobDetails = {};
                
                pobDetails.userID     = job.userID;
                pobDetails.workerID   = job.workerID;
                pobDetails.unitAmount = transaction.amount * 0.80; //Take off 80% for the workers pay			
                
                job.paymentStatusID = 2;
                
                console.log("Going to save job");
		    	
                job.save(function(err, product, numAff){
                  if(err){
                    console.log("Update Job error on payment");
                  }
                });//End of function (job.save)

                /*Get workers details for Draft invoice details inserted by dredmonds 2016.09.12*/
                req.db.User.findOne({loginID: job.workerID.toString()}, function(err, wkr){
                  if(err){
                      console.log("Find workers details error on creating draft invoice.");
                  }//End if-condition (err)

		    	  if(wkr){
		    		  var currentDate = new Date();
		    		  var dueDate = new Date();
  		    		  dueDate.setDate(dueDate.getDate() + 30); //set to 30-days payment due
  		    		  
					  pobDetails.fullName      = wkr.card.name;
                      pobDetails.email         = wkr.email;
                      pobDetails.address       = wkr.card.address;
                      pobDetails.city          = wkr.card.city;
                      pobDetails.postCode      = wkr.card.postCode;
					  pobDetails.invoiceNumber = transaction.id;
					  pobDetails.invoiceDate   = currentDate.toISOString();
					  pobDetails.dueDate       = dueDate.toISOString();
					  
                      pobDetails.description   =  "SERVICES" +
                                                  " Job ID: " + orderID +
                                                  " Worker ID: " + pobDetails.workerID +
                                                  " Account Name: " + wkr.card.name + 
                                                  " Bank Name: " + wkr.card.bankName +
                                                  " Sort Code: " + wkr.card.sortCode + 
                                                  " Account No: " + wkr.card.accountNumber + 
                                                  " IBAN: " + wkr.card.IBAN + 
                                                  " Swift Code: " + wkr.card.swiftCode; 
					  pobDetails.quantity      = "1";
					  pobDetails.unitAmount    = pobDetails.unitAmount.toFixed(2);
					  pobDetails.accountCode   = "310"; //Cost of Goods Sold
					  pobDetails.taxType       = "No VAT";
					  pobDetails.currency      = transaction.currencyIsoCode;
            
                      var strPurchaseOrder =
                        pobDetails.fullName         + ',' +
                        pobDetails.email            + ',' +
                        pobDetails.address          + ',' +
                                                    ',,,' +
                        pobDetails.city             + ',' +
                                                      ',' +
                        pobDetails.postCode         + ',' +  
                                                      ',' +
                        pobDetails.invoiceNumber    + ',' +
                        pobDetails.invoiceDate      + ',' +
                        pobDetails.dueDate          + ',' +
                                                     ',,' +
                        pobDetails.description      + ',' +
                        pobDetails.quantity         + ',' +
                        pobDetails.unitAmount       + ',' +
                        pobDetails.accountCode      + ',' +
                        pobDetails.taxType          + ',' +
                                                  ',,,,,' +
                        pobDetails.currency;
            
		    		  console.log("Preparing to write POrders values: " + strPurchaseOrder);
                      
		    		  //For Linux File Directory
		    		  var writableStrmPOrdr = fs.createWriteStream( __dirname.substring(0, __dirname.indexOf("/routes")) + '/downloads/paymentTXn/PurchaseOrderBills.csv', {flags : 'a+'});
                      
		    		  /* Inserted to handle IO error in writing with multiple instances*/
  		    		  writableStrmPOrdr.write(strPurchaseOrder + '\n', function(err){
		    		    if(err){
		    		      console.log("Error writing on Purchase Order Bill CSV file.");
		    		    }
		    		    console.log("Transaction was saved to Purchase Order Bill CSV file.");
		    		  });				  

		    		  
                  }//End if-condition(wrk)
                
                });//End of getting workers details            
		    	
              }//End if-condition (job)
              
            });//End of findOne: jobID query
			
        console.log("Return responsed on braintree transaction result = success.");
		//Payment Transaction Details Page
        res.send(sendCompleteStatus(transaction.id, orderID, amount)); //inserted by dredmonds 2016.09.12
	    return;			
         		   
	  }//End if-condition (result.success)
	  
  });//End of gatewate.transaction.sale 
  
  
};



exports.getPaypalCredentials = function(req, res){     //Paypal Credentials
  //No credential yet but soon it will emerged.
};//End getPaypalCredential Function()


exports.urlPaypalreturn = function(req,res){  //Paypal Transaction Results(http://www.ahitis.com/payment/paypalreturn)
/**
* SAMPLE RESULT STRING FROM PAYPAL
* http://www.ahitis.com/payment/paypalreturn?tx=39Y65750PC282571G&st=Completed&amt=24%2e00&cc=GBP&cm=ORD12345678ZZ&item_number=
*
**/

  //Catch the return value from paypal IPN
  var tx = req.body.tx || req.query.tx;          //Extract txn_id values generates by paypal
  var custom = req.body.cm || req.query.cm;      //Extract Paypal passthrough variable(custom) inserted from submit form.
  var amount = req.body.amt || req.query.amt;    //Extract amount value from paypal return string. //inserted by dredmonds 2016.07.12
  var result = {};
  var responseResults = {};

  console.log("tx: " +tx+" | custom: " + custom + " | amount: " + amount);
  if(tx){ //Validate Transaction ID from Paypal Data Transfer(PDT)

  	var postData = querystring.stringify({                                  //Paypal(PDT) form data
  		cmd: '_notify-synch',
  		tx: tx,
  		//at: 'GBfbQvPZjn15MU4sSAEn_XATTj03g-PtETTypeB4HHeT9whwBhbxKdx8YBG' //Sandbox Payment Data Transfer(PDT) ID
  		at: '5Ogv7E8cPWWpkzMvHMfykVvglSyYq-ghWgvG6939p134SA5vM8xCb0rDJVi'   //Live Payment Data Transfer(PDT) ID
  	});

  	var options = {                                                         //http request option
  	  //host: 'www.sandbox.paypal.com',
	  host: 'www.paypal.com',
  	  port: 443,
  	  method: 'GET',
  	  path: '/cgi-bin/webscr?' + postData
  	};

  	https.get(options, function(response){                                  //Do a GET request back to Paypal to validate txn_id
  		response.on('data', function(chunk){
  			//Chunk data and save to result
  			result += chunk.toString('utf8');
  		}).on('end', function(){
        console.log("Successful payment back from paypal");
			/*
			 * DUE TO SOME CHANGES OF RETURN RESULTS FROM HTTPS GET TO PAYPAL
			 * I AMMENDED THE PREVIOUS CODE TO AVOID SOME ERROR ADJUSTMENT IN THE FUTURE
			 * -dredmonds 2016.07.12
			 * */

  		//postData return results is 200
  		if(response.statusCode === 200){                                    //if status code is 200 extract values
          //Prepare the transaction results DB object
          var transactionResult = new req.db.Transaction({
            transactionID: tx,
            jobID: custom,
            amount: amount,
            currency: 'GBP',
            paymentDate: new Date(),
            paymentStatus: 'Completed',
            createdBy: 'Paypal(PDT)',
            createdDate: new Date(),
            updatedBy: 'PDT',
            updatedDate: new Date()
          });

          console.log("going to save transaction");
          //Save to Transaction collections.
          transactionResult.save(function(err, product, numAff){
             if (err){
               console.log("DB error on Transaction Txn ID: " + tx);
             }else{
               console.log("DB success on Transaction Txn ID: " + tx);
             }//End if condition
          });//End txnPaymentResult.save




         /* To Do, Update/Insert DB Collection here if required
          *
          * e.g. PaymentStatus and Job
          *
          * */

         console.log("Going to find job in db: " + custom);
		 req.db.Job.findOne({jobID: custom.toString()}, function(err, job){
		   if(err){
		     console.log("Can't find Job ID, error on creating draft invoice. " + err);
		   }

           if(job){
             var invoiceDetails = {};
             var purchaseOrderDetails = {};
             /* invoiceDetails.workerID   = job.workerID; //inserted by dredmonds to get workers ID for draft invoice creation 2016.07.06
             invoiceDetails.quantity   = job.duration; //inserted by dredmonds to get workers ID for draft invoice creation 2016.07.06
             invoiceDetails.unitAmount = ((job.amount / 120) * 100) / job.duration;   //inserted by dredmonds to get workers ID for draft invoice creation 2016.07.06 */
             /*Amended 2016.09.05 - dredmonds*/
             invoiceDetails.workerID   = job.workerID;
             invoiceDetails.quantity   = "1";
             invoiceDetails.unitAmount = job.amount;
             purchaseOrderDetails.quantity = job.duration;
             purchaseOrderDetails.unitAmount = ((job.amount / 120) * 100) / job.duration;
             
             
             job.paymentStatusID = 2;
             
             console.log("Going to save job");
             job.save(function(err, product, numAff){
               if(err){
                 console.log("Update Job error on payment");
               }
             });//End of job.save function

            /*Get workers details for Draft invoice details inserted by dredmonds 2016.07.06*/
            req.db.User.findOne({loginID: invoiceDetails.workerID}, function(err, wkr){
              if(err){
                  console.log("Find workers details error on creating draft invoice.");
              }
              if(wkr){
                  var dueDate = new Date();
  				  dueDate.setDate(dueDate.getDate() + 30); //set to 30-days payment due
  				  var currentDate = new Date();
  				  var contactDetails = {};
  				
                  invoiceDetails.fullName      = wkr.card.name;
                  invoiceDetails.email         = wkr.email;
                  invoiceDetails.address       = wkr.card.address;
                  invoiceDetails.city          = wkr.card.city;
                  invoiceDetails.postCode      = wkr.card.postCode;
                  invoiceDetails.invoiceNumber = tx;
                  invoiceDetails.reference     = custom;
                  invoiceDetails.invoiceDate   = currentDate.toISOString();
                  invoiceDetails.dueDate       = dueDate.toISOString();
                  invoiceDetails.description   = "Services";
                  
                  contactDetails.accountName    = wkr.card.name;
				  contactDetails.email          = wkr.email; 
                  contactDetails.firstname      = wkr.firstname;
                  contactDetails.lastname       = wkr.lastname;
                  contactDetails.address        = wkr.card.address; 
                  contactDetails.city           = wkr.card.city; 
                  contactDetails.postCode       = wkr.card.postCode;  
                  contactDetails.countryCode    = wkr.countryCode;
                  contactDetails.contactNo      = wkr.contactNo;
                  contactDetails.bankName       = wkr.card.bankName;
                  contactDetails.bankAcctNumber = wkr.card.sortCode + " " + wkr.card.accountNumber; 
                  

                  var strInvoice =
                    invoiceDetails.fullName      + ',' +
                    invoiceDetails.email         + ',' +
                    invoiceDetails.address       + ',' +
                                                 ',,,' +
                    invoiceDetails.city          + ',' +
                                                   ',' +
                    invoiceDetails.postCode      + ',' +  
                                                   ',' +
                    invoiceDetails.invoiceNumber + ',' +
                    invoiceDetails.reference     + ',' +
                    invoiceDetails.invoiceDate   + ',' +
                    invoiceDetails.dueDate       + ',,,' +
                    invoiceDetails.description   + ',' +
                    invoiceDetails.quantity      + ',' +
                    invoiceDetails.unitAmount    + ',' +
                                          ',,,,,,,,,,';
										 
                  var strPurchaseOrder =
                    invoiceDetails.fullName         + ',' +
                    invoiceDetails.email            + ',' +
                    invoiceDetails.address          + ',' +
                                                    ',,,' +
                    invoiceDetails.city             + ',' +
                                                      ',' +
                    invoiceDetails.postCode         + ',' +  
                                                      ',' +
                    invoiceDetails.invoiceNumber    + ',' +
                    invoiceDetails.invoiceDate      + ',' +
                    invoiceDetails.dueDate          + ',,,' +
                    invoiceDetails.description      + ',' +
                    purchaseOrderDetails.quantity   + ',' +
                    purchaseOrderDetails.unitAmount + ',' +
                                               ',,,,,,,,';

                  var strContacts =                  
                    contactDetails.accountName   + ',,' +
					contactDetails.email          + ',' +
					contactDetails.firstname      + ',' +
					contactDetails.lastname      + ',,' +
					contactDetails.address     + ',,,,' +
					contactDetails.city          + ',,' +
					contactDetails.postCode       + ',' +
					contactDetails.countryCode    + ',' +
					                      ',,,,,,,,,,,' +
                    contactDetails.contactNo    + ',,,' +
					contactDetails.bankName       + ',' +
					contactDetails.bankAcctNumber + ',' +
					             ',,,,,,,,,,,,,,,,,,,,,';
								 
										 
  				  console.log("Preparing to write Invoice values: " + strInvoice);
				  console.log("Preparing to write POrders values: " + strPurchaseOrder);
				  console.log("Preparing to write Contacts values: " + strContacts);
  				  
  				  var writableStrmInvce = fs.createWriteStream( __dirname.substring(0, __dirname.indexOf("/routes")) + '/downloads/paymentTXn/SalesInvoiceDraft.csv', {flags : 'a+'});
				  var writableStrmPOrdr = fs.createWriteStream( __dirname.substring(0, __dirname.indexOf("/routes")) + '/downloads/paymentTXn/PurchaseOrderBills.csv', {flags : 'a+'});
  				  var writableStrmCntct = fs.createWriteStream( __dirname.substring(0, __dirname.indexOf("/routes")) + '/downloads/contacts/Contacts.csv', {flags : 'a+'});
                  
				  /* Inserted to handle IO error in writing with multiple instances*/
				  
  				  writableStrmInvce.write(strInvoice + '\n', function(err){
				    if(err){
				      console.log("Error writing on SalesInvoice CSV file.");
				    }
				    console.log("Transaction was saved to SalesInvoice CSV file.");
				  });
                  
  				  writableStrmPOrdr.write(strPurchaseOrder + '\n', function(err){
				    if(err){
				      console.log("Error writing on Purchase Order Bill CSV file.");
				    }
				    console.log("Transaction was saved to Purchase Order Bill CSV file.");
				  });				  
				  
				  writableStrmCntct.write(strContacts + '\n', function(err){
				    if(err){
				      console.log("Error writing on Contacts CSV file.");
				    }
				    console.log("Transaction was saved to Contacts CSV file.");
				  });
				  

              }//End if-condition(wrk)

             });//End of getting workers details


           }//End if condition (job)

         });//End of findOne: jobID query


          /*To Do, POST Txn results to third party accounting software.
           *
           * Insert more codes to post data
           *
           * */
          
          
          //Redirect to baseURl+/user/payment/status?jobID=value&txnID=value&amt=value&status=value
          res.send(sendCompleteStatus(tx, custom, amount)); //inserted by dredmonds 2016.07.12
	      return;

  		}//End If condition (res.statusCode === 200)

  	  });//End .on('end')

  	}).on('error', function(err){
  		//GET method error, can't retrieved transaction id

        //Prepare the transaction results DB object
        var transactionResult = new req.db.Transaction({
        	transactionID: tx,
        	jobID: custom,
        	amount: amount,
        	currency: 'GBP',
        	paymentDate: new Date(),
        	paymentStatus: 'Data Error',
        	createdBy: 'Paypal(PDT)',
        	createdDate: new Date(),
        	updatedBy: 'PDT',
        	updatedDate: new Date()
        });


        //Save to Transaction collections.
        transactionResult.save(function(err, product, numAff){
           if (err){
           	 console.log("Data error on Transaction Txn ID: " + tx);
           }else{
           	 console.log("Data error on Transaction Txn ID: " + tx);
           }//End if condition
        });//End txnPaymentResult.save

  		console.log("Txn GET error: " + err);
  	    //res.redirect(307, 'http://localhost:8100/#/user/payment/status?jobID=' + responseResults.custom + '&txnID=Error' + '&amt=0.0' + '&status=Error');
  		res.send(sendTransactionError(tx, custom, "0.00")); //inserted by dredmonds 2016.07.12
  	    return;

  	});//End http GET


  }else{
  	//No transaction ID then force to redirect to payment page
	console.log("No Transaction ID.");
	//res.redirect(307, 'http://localhost:8100/#/user/payment/main?jobID=' + custom);
	res.send(sendTransactionError(tx, custom, "0.00")); //inserted by dredmonds 2016.07.12
	return;

  }//End if(tx) condition

};//End urlPaypalReturn Function()


function sendTransactionError(tx,jobID,amt){
  var statusPage =
  '<div style="height: 40px;"></div>' +
  '<div style="height: 40px;"></div>' +
  '<div style="height: 40px;"></div>' +
  '<div style="text-align:center">' +
  '<h2>Your payment transaction has an error<h2>' +
  '<div>Payment Details</div>' +
  '<div>Payment Reference: </div>' +
  '{{payment.reference}}' +
  '<div>Job Order ID: </div>' +
  '{{payment.jobID}}' +
  '<div>Amount: </div>' +
  '{{payment.amount}}' +
  '<h3>Please tab "Done" below to close this page.</h3>' +
  '<h3>or</h3>' +
  '<h3>Press your Android back button to close this page.</h3>' +
  '</div>';

  statusPage = statusPage.replace("{{payment.reference}}", tx).replace("{{payment.jobID}}", jobID).replace("{{payment.amount}}", amt);
  return statusPage;
}


function sendCancelStatus(){
  var statusPage =
  '<div style="height: 40px;"></div>' +
  '<div style="height: 40px;"></div>' +
  '<div style="height: 40px;"></div>' +
  '<div style="text-align:center">' +
  '<h2>Your payment has been canceled</h2>' +
  '<div style="height: 40px;"></div>' +
  '<div style="height: 40px;"></div>' +
  '<div style="height: 40px;"></div>' +
  '<div style="height: 40px;"></div>' +
  '<div style="height: 40px;"></div>' +
  '<div style="height: 40px;"></div>' +
  '<h3>Please tab "Done" below to close this page.</h3>' +
  '<h3>or</h3>' +
  '<h3>Press your Android back button to close this page.</h3>' +
  '</div>';

  return statusPage;
}

function sendCompleteStatus(tx, jobID, amt){
  var statusPage =
  '<div style="height: 40px;"></div>' +
  '<div style="height: 40px;"></div>' +
  '<div style="height: 40px;"></div>' +
  '<div style="text-align:center">' +
  '<h2>Your payment is completed<h2>' +
  '<div>Payment Details</div>' +
  '<div>Payment Reference: </div>' +
  '{{payment.reference}}' +
  '<div>Job Order ID: </div>' +
  '{{payment.jobID}}' +
  '<div>Amount: </div>' +
  '{{payment.amount}}' +
  '<h3>Please tab "Done" below to close this page.</h3>' +
  '<h3>or</h3>' +
  '<h3>Press your Android back button to close this page.</h3>' +
  '</div>';

  statusPage = statusPage.replace("{{payment.reference}}", tx).replace("{{payment.jobID}}", jobID).replace("{{payment.amount}}", amt);
  return statusPage;
}

exports.urlPaypalcancel = function(req, res){  //Paypal Transaction Cancel(http://www.ahitis.com/payment/paypalcancel)
  //Redirect the page back to ahitis mobile application.
  console.log("Paypal cancel redirect.");
  var custom = req.body.cm || req.query.cm;

  if(!custom){
    console.log(JSON.stringify(req.query));
  }
  console.log("Payment Canceled, Job ID returned: " + custom);
  req.db.Job.findOne({jobID: custom}, function(err, job){
	   if(err){
	     console.log("Find Job error on cancel payment");
	   }

    if(job){
 	    job.paymentStatusID = 3;

 	    job.save(function(err, product, numAff){

 		     if(err){
	 	  	  console.log("Update Job error on cancel payment");
 	       }

 	     });//End of job.save function

    }//End if condition (job)

  });//End of findOne: jobID query

  res.send(sendCancelStatus());
  return;

};//End urlPaypalcancel Function()
