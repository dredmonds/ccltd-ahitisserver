/*******************************************************************************
 * Module Name: job information router.
 * Description: job information router of Ahitis server. (This router is used for all job related process.)
 * Author: Brian Lai Date Modified: 2016.04.20.
 * Related files: n/a.
 ******************************************************************************/
var notification = require('../routes/notification');

//This route is used for listing all jobs by worker.
exports.workerList = function(req, res) {
	var loginID = req.loginID;
	var scope = req.query.scope;
	var result = {
			result: "",
			jobs: []
		};

			var query = req.db.Job.find({workerID: loginID});
			if(scope === "current"){
				query.where('statusID').in([1,2]);
			}else if(scope==="history"){
				query.where('statusID').in([3,4,5]);
			}else if(scope==="dispute"){
				query.where('statusID').in([1,2,3,4,5]);
			}
			query.sort({createdDate: -1});
			query.exec(function(err,jobs){
				if(err){
					result.result = "500";
					res.status(500).json(result);
					return;
				}

				if(jobs && jobs.length > 0){
					jobs.forEach(function(job){
						var jobObj = {};
						jobObj.jobID = job.jobID;
						jobObj.userID = job.userID;
						jobObj.workerID = job.workerID;
						jobObj.skillID = job.skillID;
						jobObj.loc = job.loc;
						jobObj.statusID = job.statusID;
						jobObj.paymentStatusID = job.paymentStatusID;
						jobObj.createdDate = job.createdDate;
						result.jobs.push(jobObj);
					});
					console.log("jobs DB query success.");
					res.status(200).json(result);
					return;
				}else{
					result.result = "No jobs Found.";
					console.log("No jobs for worker.");
					res.status(200).json(result);
					return;
				}
			});


};

//This route is used for listing all jobs by user.
exports.userList = function(req, res) {
	var loginID = req.loginID;
	var scope = req.query.scope;
	var result = {
			result: "",
			jobs: []
		};
	console.log("Scope="+req.query.scope);
	var query = req.db.Job.find({userID: loginID});
	if(scope === "current"){
		query.where('statusID').in([1,2]);
	}else if(scope==="history"){
		query.where('statusID').in([3,4,5]);
	}else if(scope==="dispute"){
		query.where('statusID').in([1,2,3,4,5]);
	}
	query.sort({createdDate: -1});
	query.exec(function(err,jobs){
		if(err){
			result.result = "500";
			res.status(500).json(result);
			return;
		}

		if(jobs){
			jobs.forEach(function(job){
				var jobObj = {};
				jobObj.jobID = job.jobID;
				jobObj.userID = job.userID;
				jobObj.workerID = job.workerID;
				jobObj.skillID = job.skillID;
				jobObj.loc = job.loc;
				jobObj.statusID = job.statusID;
				jobObj.paymentStatusID = job.paymentStatusID;
				jobObj.createdDate = job.createdDate;
				result.jobs.push(jobObj);
			});
			console.log("jobs DB query success.");
			res.status(200).json(result);
			return;
		}else{
			result.result = "No jobs Found.";
			console.log("No jobs for worker.");
			res.status(200).json(result);
			return;
		}
	});
};

exports.jobID = function(req,res){
	var loginID = req.loginID;
	var jobID = req.body.jobID;
	var result = {
			result: "",
			currentDate: null,
			hours: 2,
			job: null
		};

	req.db.Job.findOne({jobID: jobID}, function(err,job){
		if(err){
			result.result = "500";
			res.status(500).json(result);
			return;
		}

		if(job){
			var jobObj = {};
			jobObj.jobID = job.jobID;
			jobObj.userID = job.userID;
			jobObj.workerID = job.workerID;
			jobObj.skillID = job.skillID;
			jobObj.loc = job.loc;
			jobObj.countryCode = job.countryCode;
			jobObj.statusID = job.statusID;
			jobObj.paymentStatusID = job.paymentStatusID;
			jobObj.createdDate = job.createdDate;
			result.job = jobObj;
			result.currentDate = new Date();
			result.hours = Math.ceil((result.currentDate - jobObj.createdDate)/1000/3600/24);
			console.log(jobObj.createdDate + " - " + result.hours);
			if(result.hours < 2){
				result.hours = 2;
			}
			console.log("job DB query success.");
			res.status(200).json(result);
			return;
		}else{
			result.result = "No job Found.";
			console.log("No job for worker.");
			res.status(200).json(result);
			return;
		}
	});
};

exports.jobPaymentDetail = function(req,res){
	var loginID = req.loginID;
	var jobID = req.body.jobID;
	var result = {
			result: "",
			amount: null,
			hours: 2
		};

	req.db.Job.findOne({jobID: jobID}, function(err,job){
		if(err){
			result.result = "Find Job error";
			console.log("Find job error on jobPaymentDetail");
			res.status(500).json(result);
			return;
		}

		if(job){
			result.hours = job.duration;
			result.amount = job.amount;
			console.log("job payment detail success.");
 			res.status(200).json(result);
 			return;
		}else{
			result.result = "No job Found.";
			console.log("No job found.");
			res.status(500).json(result);
			return;
		}
	});
}

exports.jobComplete = function(req,res){
	var loginID = req.loginID;
	var jobID = req.body.jobID;
	var skillPrice = req.body.skillPrice;
	var amount = 0;
	var result = {
			result: "",
			currentDate: null,
			hours: 2
		};

	req.db.Job.findOne({jobID: jobID}, function(err,job){
		if(err){
			result.result = "Find Job error";
			console.log("Find job error on jobComplete");
			res.status(500).json(result);
			return;
		}

		if(job){
			result.currentDate = new Date();
 			result.hours = Math.ceil((result.currentDate - job.createdDate)/1000/3600/24);
 			if(result.hours < 2){
 				result.hours = 2;
 			}
 			amount = skillPrice * result.hours;
 			result.amount = amount;

 			console.log(job.createdDate + " - " + result.hours);

 			job.statusID = 3;
			job.paymentStatusID = 3;
 			job.duration = result.hours;
 			job.amount = amount;

		 	job.save(function(err, product, numAff){

		 		if(err){
		 			console.log("Update Job error on Complete Job");
		 	    }

					req.db.User.findOne({loginID: job.workerID}, function(err, worker){
						if(err){
							console.log("Error find worker in Complete Job");
							}

						if(worker){
							if(worker.skill.duration == null){
								worker.skill.duration = 0;
							}
							if(worker.skill.amount == null){
								worker.skill.amount = 0;
							}

							worker.skill.duration = worker.skill.duration + result.hours;
							worker.skill.amount = amount;

							worker.save(function(err, product, numAff){
								if(err){
									console.log("Save worker duration and amount error");
								}

								req.db.OnlineWorker.findOne({jobID: jobID}, function(err, oWorker){
									if(err){
										console.log("Error find online worker in Complete Job");
									}

									if(oWorker){
						 				oWorker.jobID = null;
										oWorker.save(function(err, product, numAff){
							 				if(err){
							 					console.log("Remove job from worker error");
							 				}
							 			});
									}else{
										console.log("Worker is already offline");
									}
									console.log("job complete success.");
									res.status(200).json(result);
									return;
						 		});
							});
						}else{
							//No this worker?
							result.result = "Find worker error";
							console.log("Find worker error on jobComplete");
							res.status(500).json(result);
							return;
						}
					});
		 	});//End of job.save function
		}else{
			result.result = "No job Found.";
			console.log("No job to complete.");
			res.status(500).json(result);
			return;
		}
	});
};

exports.jobAccept = function(req,res){
	var loginID = req.loginID;
	var jobID = req.body.jobID;

	var result = {
			result: ""
		};

	console.log("Worker " + req.loginID + " going to accept job " + req.body.jobID);
	req.db.Job.findOne({jobID: jobID}, function(err,job){
		if(err){
			result.result = "Job accept fail";
			res.status(500).json(result);
			return;
		}

		if(job){
			console.log("Job found.");
			if(job.statusID == 1){
				job.statusID = 2;
				job.workerID = loginID;

				//Update Accept and send notification to User
				job.save(function(err, product, numAff) {
		    		if (err) { //DB error
		    			console.log("Error accept job for user: " + product.userID);
		    			result.result = "Accept job error.";
		    			res.status(500).json(result);
		    			return;
		    		}
		    		try{
		    			notification.send(req, product.userID, "Job Accepted: An Ahitis worker accepted your job.", product.jobID.toHexString());
    	    		}catch(err){
    	    			result.result = "Notification send error.";
    	    			res.status(500).json(result);
    	    			return;
    	    		}

							console.log("going to mark job in onlineworker");
							req.db.OnlineWorker.findOne({loginID: loginID}, function(err, worker){
								if(err){
									result.result = "Search Online Worker error";
									console.log("Search onlineWorker error");
									res.status(500).json(result);
									return;
								}

								if(worker){
									console.log("Onlineworker found");
									worker.jobID = jobID;
									worker.save(function(err, wProduct, wNumAff) {
							    		if (err) { //DB error
							    			console.log("Error assign JobID in online worker: " + worker.loginID);
							    			result.result = "accept Online Worker error.";
							    			res.status(500).json(result);
							    			return;
							    		}


											result.result = "Job accept success";
											console.log("job accept success.");
											res.status(200).json(result);
											return;
									});
								}else{
									result.result = "No worker found.";
									res.status(204).json(result);
									return;
								}
							});
		    	});
			}else{
				result.result = "Job is already accepted by another worker.";
				console.log("job accepted already.");
				res.status(204).json(result);
			}
		}else{
			result.result = "No job Found.";
			console.log("No job Found.");
			res.status(500).json(result);
			return;
		}
	});
};

exports.jobReject = function(req,res){
	var loginID = req.loginID;
	var jobID = req.body.jobID;

	var result = {
			result: ""
		};

	req.db.Job.findOne({jobID: jobID}, function(err,job){
		if(err){
			result.result = "Job reject fail";
			res.status(500).json(result);
			return;
		}

		if(job){
			if(job.statusID === 1){
				job.statusID = 5;

				//Update Accept and send notification to User
				job.save(function(err, product, numAff) {
					if (err) { //DB error
						console.log("Error reject job for user: " + product.userID);
						result.result = "Reject job error.";
						res.status(500).json(result);
						return;
					}
					try{
						notification.send(req, product.userID, "Job Rejected: All ahitis worker rejected your job.", product.jobID.toHexString());
    	    		}catch(err){
    	    			result.result = "Notification send error.";
    	    			res.status(500).json(result);
    	    			return;
    	    		}

					result.result = "Rejected";
					console.log("job reject success.");
					res.status(200).json(result);
					return;
				});
			}else{
				result.result = "The job is not pending.";
				console.log("Not rejected");
				res.status(200).json(result);
				return;
			}
		}else{
			result.result = "No job Found.";
			console.log("No job Found.");
			res.status(200).json(result);
			return;
		}
	});
};
