/*****************************************************************
* Module Name: dispute router
* Description: dispute router of Ahitis server
* Author: HR
* Date Modified: 2016.06.30
* Related files: n/a
*****************************************************************/
var mongoose = require('mongoose');
var nodemailer = require('nodemailer');
//This router is to register a dispute
exports.registerDispute = function(req, res) {
	//Grab object send by client device.
	var loginID = req.loginID;
	var disputedJob = req.body.jobID;
	var disputedReason = req.body.reason;
	var result = {
		result : ""
	};
	console.log("New Dispute: " + ">" + disputedJob + "<" + ' ' + ">" + disputedReason + "<" + ' ' + ">" + loginID + "<");

	var regDispute = new req.db.Dispute({
		jobID: mongoose.mongo.ObjectId(disputedJob),
		reason: disputedReason,
		createdBy: loginID,
		createdDate: new Date(),
		updatedBy: "",
		updatedDate: new Date()
	});
	//Save to DB
	regDispute.save(function(err, product, numAff) {
		if (err) {
			console.log("Error registering dispute " + disputedJob);
			console.log(err.message);
			console.log(err.stack);
			result.result = "Dispute error.";
			res.status(500).json(result);
			return;
		}

		var disputeID = product.disputeID;

		req.db.Job.findOne({jobID: disputedJob}, function(err, job){
			if(err){
				console.log("Find Job error on cancel payment");
			    result.result = "Dispute error.";
			    res.status(500).json(result);
			}

		    if(job){
		      job.statusID = 4;

		 	  job.save(function(err, jobProduct, jobNumAff){
		 		  	if(err){
		 		  		console.log("Update Job error on cancel payment");
		 		  		result.result = "Dispute error.";
		 		  		res.status(500).json(result);
		 		  	}

		 		  	//parallel send email
		 		  	req.db.User.findOne({loginID: loginID}, function(err, user){
		 	       	    if(err){
			 	       	      console.log("User not found");
			 	       	      result.result = "Dispute error.";
			 	       	      res.status(500).json(result);
		 	       	    }

									if(user){
										// create reusable transporter object using the default SMTP transport
										var transporter = nodemailer.createTransport('smtps://admin%40ahitis.com:DijudAm8=@web044.dnchosting.com');

										// setup e-mail data with unicode symbols
										var mailOptions = {
														    from: '"AHitis Admin" <admin@ahitis.com>', // sender address
														    to: user.email + ", ahitis@cc-ltd.com", // list of receivers
														    subject: user.firstname + ' dispute for jobID: ' + disputedJob, // Subject line
														    html:'<html><head><meta content="text/html;charset=UTF-8" http-equiv="content-type" /><title></title></head><body><p>Hi ' + user.firstname + '!</br>Your dispute has been sent to our customer service. </br>Your dispute ID is: ' + disputeID.toString() + '</br>Your dispute reason: ' + disputedReason + '</br>Thanks,</br>The AHitis team</p></body></html>'// plaintext body
														};

										// send mail with defined transport object
										transporter.sendMail(mailOptions, function(error, info){
										    if(error){
										        console.log(error);
										    }else{
										    	console.log('Message sent: ' + info.response);
										    }
										});
		 	       	    }
		 		  	});

		 		  	//Parallel free worker
		 		  	req.db.OnlineWorker.findOne({jobID: disputedJob}, function(err, worker){
		 	       	    if(err){
		 	       	      console.log("Find online worker error");
		 	       	      result.result = "Dispute error.";
		 	       	      res.status(500).json(result);
		 	       	    }
		 	       	    if(worker){
		 	       	    	worker.jobID = null;
		 	       	    	worker.save(function(err, product, numAff){
		 	       	    		if(err){
		 	       	    			console.log("Remove job from worker error");
		 			 	       	    result.result = "Remove worker error.";
		 			 	       	    res.status(500).json(result);
		 	       	    		}

		 	       	    		result.result = "Job has been disputed.";
		 	       	    		res.status(200).json(result);
		 	       	    		return;
		 	       	    	});
		 	       	   }else{
	 	       	    		result.result = "Job has been disputed.";
	 	       	    		res.status(200).json(result);
	 	       	    		return;
		 	       	   }
		 	        });

		 	  });//End of job.save function

		    }//End if condition (job)
		    else{
				console.log("No job found");
			    result.result = "No job found";
			    res.status(500).json(result);
		    }
		  });//End of findOne: jobID query
	});
};
