/*******************************************************************************
 * Module Name: skill information router.
 * Description: skill information router of Ahitis server. (This router is used for all skill related process.)
 * Author: Brian Lai Date Modified: 2016.04.07.
 * Related files: n/a.
 ******************************************************************************/

//This route is used for listing all skills.
exports.listName = function(req, res) {
	//Prepare for response object
	var result = {
		result: "",
		skills: []
	};
	//DB query to worker to get user's information
	req.db.Skill.aggregate([{$match:{ranking:0}},{$group:{_id:{skillid:"$skillid", name:"$name", icon:"$icon", unit:"$unit"}}},{"$project":{_id:0, skillid:"$_id.skillid",name:"$_id.name",icon:"$_id.icon",unit:"$_id.unit"}},{$sort:{skillid:1}}]).exec(function(err, skills) {
		if(err){ //DB Error
			result.result = "DB error.";
			console.log("skills DB query fail.");
			res.status(500).json(result);
			return;
		}

		if(skills && skills.length > 0){
			skills.forEach(function(skill){
				var skillObj = {};
				skillObj.skillid = skill.skillid;
				skillObj.name = skill.name;
				skillObj.unit = skill.unit;
				skillObj.icon = skill.icon;
				result.skills.push(skillObj);
			});
			console.log("skills DB query success.");
			res.status(200).json(result);
			return;
		}else{
			result.result = "No Skills Found";
			console.log("No skills in DB.");
			res.status(500).json(result);
			return;
		}
	});
};

exports.skillID = function (req,res){
	var skillID = req.body.skillID;
	var result = {
			result: "",
			skill: {}
		};
	req.db.Skill.findOne({skillid: parseInt(skillID), ranking: 0}, function(err,skill){
		if(err){
			result.result = "DB error.";
			console.log("skill DB query fail.");
			res.status(500).json(result);
			return;
		}

		if(skill){
			console.log("SkillID: " + skill.skillid);
			result.skill.skillID = skill.skillid;
			result.skill.name = skill.name;
			result.skill.description = skill.description;
			result.skill.ranking = skill.ranking;
			result.skill.unit = skill.unit;
			result.skill.sandboxID = skill.sandboxID;
			result.skill.liveID = skill.liveID;

			result.result = "Skill find";
			console.log("skill DB query success");
			res.status(200).json(result);
		}
	});
}
