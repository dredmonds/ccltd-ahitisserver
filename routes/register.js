/*****************************************************************
* Module Name: register router
* Description: register router of Ahitis server
* Author: Brian Lai
* Date Modified: 2016.01.04
* Related files: n/a
*****************************************************************/
var md5 = require('md5');
var nodemailer = require('nodemailer');

//This router is to register a user/worker.
exports.user = function(req, res) {
	//Grab object send by client device.
	var username = req.body.username;
	var password = req.body.password;
	var firstname = req.body.firstname;
	var lastname = req.body.lastname;
	var email = req.body.email;
	var contactNo = req.body.contactNo;
	var userType = req.body.userType;
	var countryCode = req.body.countryCode;
	var skill = req.body.skill;

	//Prepare the response object.
	var result = {
		result : ""
	};

	console.log("User " + username + " trying to register...");

	req.db.User.findOne({loginID: username}, function(err,user){
		if(err){
			console.log("Error reading User: " + username);
			result.result = "Register error.";
			res.status(500).json(result);
			return;
		}

		if(user){
			console.log("User already exists: " + username);
			result.result = "User is already exists";
			res.status(500).json(result);
			return;
		}else{
			//Prepare the DB object
			var regUser = new req.db.User({
				loginID : username,
				password : password,
				firstname : firstname,
				lastname : lastname,
				email : email,
				contactNo : contactNo,
				userType: userType,
				countryCode: countryCode,
				skill: {
					skillID: skill,
					ranking: 0,
					duration: 0,
					amount: 0
				},
				card: {},
				activationKey: md5(username + new Date().getTime()),
				activationStatus: 0,
				createdBy: "REG SYSTEM",
				createdDate: new Date(),
				updatedBy: "",
				updatedDate: new Date()
			});

			//Save to DB
			regUser.save(function(err, product, numAff) {
				if (err) {//DB Error
					console.log("Error registering user " + username);
					console.log(err.message);
					console.log(err.stack);
					result.result = "Register error.";
					res.status(500).json(result);
					return;
				}

				// create reusable transporter object using the default SMTP transport
				var transporter = nodemailer.createTransport('smtps://admin%40ahitis.com:DijudAm8=@web044.dnchosting.com');

				// setup e-mail data with unicode symbols
				var mailOptions = {
								    from: '"AHitis Admin" <admin@ahitis.com>', // sender address
								    to: email, // list of receivers
								    subject: firstname + ', please activate your AHitis account', // Subject line
								    html:'<html><head><meta content="text/html;charset=UTF-8" http-equiv="content-type" /><title></title></head><body><p>Hi ' + firstname + '!</br>Your AHitis activation code is <b>' + regUser.activationKey + '</b>.</br>To activate your account, please enter the code into the form during first login AHitis application.</br>Thanks,</br>The AHitis team</p></body></html>'// plaintext body
								};

				// send mail with defined transport object
				transporter.sendMail(mailOptions, function(error, info){
				    if(error){
				        console.log(error);
				    	result.result = "Email send error.";
				    	res.status(500).json(result);
				    }else{
				    	console.log('Message sent: ' + info.response);
				    	//Success
				    	result.result = "User " + username + " registered. You have to activate your account by entering the activation code after first login.";
				    	res.status(200).json(result);
				    }
				});

			});
		}
	})
};

exports.depositCard = function(req,res){
	var loginID = req.loginID;
	var DBSref = req.body.DBSref;
	var cardName = req.body.cardName;
	var cardAddress = req.body.cardAddress;
	var cardCity = req.body.cardCity;
	var cardPostCode = req.body.cardPostCode;
	var cardDay = req.body.cardDay;
	var cardMonth = req.body.cardMonth;
	var cardYear = req.body.cardYear;
	var cardBankName = req.body.cardBankName;
	var cardAccountNumber = req.body.cardAccountNumber;
	var cardSortCode = req.body.cardSortCode;
	var cardIBAN = req.body.cardIBAN;
	var cardSwiftCode = req.body.cardSwiftCode;

	var result = {
		result: ""
	};

	//Search for the user in Database
	req.db.User.findOne({loginID: loginID}, function(err, user){
		if(err){ //DB error
			result.result = "Login error.";
			res.status(500).json(result);
			return;
		}

		if(user){ //User found.
			user.card = {};
			user.card.DBSref = DBSref;
			user.card.name = cardName;
			user.card.address = cardAddress;
			user.card.city = cardCity;
			user.card.postCode = cardPostCode;
			user.card.day = cardDay;
			user.card.month = cardMonth;
			user.card.year = cardYear;
			user.card.bankName = cardBankName;
			user.card.accountNumber = cardAccountNumber;
			user.card.sortCode = cardSortCode;
			user.card.IBAN = cardIBAN;
			user.card.swiftCode = cardSwiftCode;

			user.save(function(err, product, numAff) {
				if (err) {
					console.log("Update deposit card error");
					result.result = "Update deposit card error.";
					res.status(500).json(result);
					return;
				}
				res.status(200).json(result);
				return;
			});
		}
	});
};


//Update worker's bank details and DBS number
exports.updateDepositCard = function(req, res) {             /*inserted by dredmonds 2016.07.08*/
	var loginID = req.loginID;
	var DBSref = req.body.DBSref;
	var cardName = req.body.cardName;
	var cardAddress = req.body.cardAddress;
	var cardCity = req.body.cardCity;
	var cardPostCode = req.body.cardPostCode;
	var cardDay = req.body.cardDay;
	var cardMonth = req.body.cardMonth;
	var cardYear = req.body.cardYear;
	var cardBankName = req.body.cardBankName;
	var cardAccountNumber = req.body.cardAccountNumber;
	var cardSortCode = req.body.cardSortCode;
	var cardIBAN = req.body.cardIBAN;
	var cardSwiftCode = req.body.cardSwiftCode;

	var result = {
		result: ""
	};

	console.log("Worker " + loginID + " trying to updates bank details and DBS no...");
	

	//Prepare the DB object
	var updtWorkerCard = {
      card: {
        DBSref: DBSref,
        name: cardName,
        address: cardAddress,
        city: cardCity,
        postCode: cardPostCode,
        day: cardDay,
        month: cardMonth,
        year: cardYear,
        bankName: cardBankName,
        accountNumber: cardAccountNumber,
        sortCode: cardSortCode,
        IBAN: cardIBAN,
        swiftCode: cardSwiftCode
	  },
	    updatedBy: req.logID,
	    updatedDate: new Date(),
	  };

	//Update User DB
	req.db.User.update({loginID: req.loginID},{$set: updtWorkerCard}, {multi:true}, function(err, product, numAff) {
		if (err) {//DB Error
			console.log("Error updating user card details and DBS no " + loginID);
			console.log(err.message);
			result.result = "Update error.";
			res.status(500).json(result);
			return;
		}
		//Success
		result.result = "Worker bank detail and DBS " + loginID + " updated.";
		res.status(200).json(result);
	});/*end of DB update*/

};  /*End of Update worker's bank details and DBS number*/


//Retrive Workers Deposit Card Information
exports.getDepositCard = function(req, res) {
	
	//Prepare for response object
	var result = {
		result: "",
		card: {}
	};
	//DB query to worker to get user's information
	req.db.User.findOne({loginID: req.loginID}, function(err, logID) {
		if(err){ //DB Error
			result.result = "DB error.";
			res.status(500).json(result);
			return;
		}

		if(logID){
			if(logID.loginID === "") {
				result.result = "Worker's card information DB query are empty.";
				res.status(403).json(result);
				return;
			} else {
				
			    result.DBSref            = logID.card.DBSref;
			    result.cardName          = logID.card.name;
			    result.cardAddress       = logID.card.address;
			    result.cardCity          = logID.card.city;
			    result.cardPostCode      = logID.card.postCode;
			    result.cardDay           = logID.card.day;
			    result.cardMonth         = logID.card.month;
			    result.cardYear          = logID.card.year;
			    result.cardBankName      = logID.card.bankName;
			    result.cardAccountNumber = logID.card.accountNumber;
			    result.cardSortCode      = logID.card.sortCode;       
			    result.cardIBAN          = logID.card.IBAN;           
			    result.cardSwiftCode     = logID.card.swiftCode;      
			    console.log("worker's bank details DB query success.");
			    res.status(200).json(result);
			    return;
			 }
		  }//Enf if(logID)
		});	
	
};//End of getDepositCard function




/*//Inserted by dredmonds for profile picture registration into DB.
exports.saveProfilePict = function(req,res){
	var loginID = req.loginID;
	var profilePict = req.baseURL + "/uploads/profilePict/" + loginID + "-" + req.body.profilePict;
	var result = {
		result: ""
	};

	//Search for the user in Database
	req.db.User.findOne({loginID: loginID}, function(err, user){
		if(err){ //DB error
			result.result = "Login error.";
			res.status(500).json(result);
			return;
		}

		if(user){ //User found.
			user.profilePict = profilePict;
			user.save(function(err, product, numAff) {
				if (err) {
					console.log("Update db profile picture error");
					result.result = "Update db profile picture error.";
					res.status(500).json(result);
					return;
				}
				res.status(200).json(result);
				return;
			});
		}
	});
};*/
