/*******************************************************************************
 * Module Name: Uploads module
 * Description: All uploaded files will be routed to here
 * Author: Brian Lai Date Modified: 2016.06.03
 * Related files: n/a.
 ******************************************************************************/
var path = require('path');

//This route is used for upload deposit card photo
exports.depositCard = function(req, res, next) {
	  res.status(200).end();
};

exports.getImage = function(req,res,next){
	var result = {
		result: ""
	};

	console.log('request starting image...');

  var fileName = req.params.name;

	console.log(path.resolve("uploads/depositCard/" + fileName));
  res.sendFile(path.resolve("uploads/depositCard/" + fileName));


};


//This route is used for profile picture uploading, inserted by dredmonds 13/06/2016
exports.saveProfilePict = function(req, res, next) {
	  res.status(200).end();
};//End of depositCard function

exports.getProfilePict = function(req,res,next){
	var result = {
		result: ""
	};

	console.log('request starting image...');

  var fileName = req.params.name;

  console.log(path.resolve("uploads/profilePict/" + fileName));
  res.sendFile(path.resolve("uploads/profilePict/" + fileName));
};//End of getProfilePict function
