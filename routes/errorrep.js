/*****************************************************************
* Module Name: errorrep router
* Description: errorrep router of Ahitis server
* Author: HR
* Date Modified: 2016.07.06
* Related files: n/a
*****************************************************************/
var mongoose = require('mongoose');
var nodemailer = require('nodemailer');
//This router is to register a errorrep
exports.registerErrorRep = function(req, res) {
	//Grab object send by client device.
	var loginID = req.loginID;
	var loginEmail = req.email;
	var errorDescription = req.body.errorRep;
	var result = {
		result : ""
	};
	console.log("New Error Description: " + ">" + errorDescription + "<" + ' ' + ">" + loginID + "<");

	var regError = new req.db.ErrorRep({
		errorRep: errorDescription,
		createdBy: loginID,
		createdDate: new Date(),
		updatedBy: "",
		updatedDate: new Date()
	});
	//Save to DB
	regError.save(function(err, product, numAff) {
		if (err) {
			console.log("Error registering error Report " + errorRepID);
			console.log(err.message);
			console.log(err.stack);
			result.result = "Error Registering Error";
			res.status(500).json(result);
			return;
		}
		req.db.User.findOne({loginID: loginID}, function(err, user){
			if(err){
				console.log("User not found");
				result.result = "Error report error.";
				res.status(500).json(result);
				return;
			}

			if(user){
				var transporter = nodemailer.createTransport('smtps://admin%40ahitis.com:DijudAm8=@web044.dnchosting.com');
				var mailOptions = {
					from: '"AHitis Admin" <admin@ahitis.com>', // sender address
					to: user.email + ", ahitis@cc-ltd.com", // list of receivers
					subject: user.firstname + ' Error Report: ', // Subject line
					html:'<html><head><meta content="text/html;charset=UTF-8" http-equiv="content-type" /><title></title></head><body><p>Hi ' + user.firstname + '!</br>Your error report has been sent to our customer service. </br>' + '</br>Your error report: ' + errorDescription + '</br>Thanks,</br>The AHitis team</p></body></html>'// plaintext body
					};

					transporter.sendMail(mailOptions, function(error, info){
						if(error){
							console.log(error);
							result.result = "Error Registering Error";
							res.status(500).json(result);
							return;
						}else{
							console.log('Message sent: ' + info.response);
							result.result = "Error report sent";
							res.status(200).json(result);
							return;
						}
					});
				}
			});
	});
};
