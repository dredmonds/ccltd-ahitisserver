/*******************************************************************************
 * Module Name: Ahitis notification service. Description: All notification
 * related functions Author: Brian Lai Date Modified: 2016.01.20. Related files:
 * /ahitis_modules/apns.js
 ******************************************************************************/
var apns = require('../ahitis_modules/apns');
var gcms = require('../ahitis_modules/gcms');

exports.reg = function(req, res) {
	var pushID = req.body.pushID;

	// Prepare the response object.
	var result = {
		result : ""
	};

	req.db.AccessToken.findOne({
		accessToken : req.body.accessToken
	}, function(err, at) {
		if (err) { // DB Error
			console.log("(RegPush) DB error.");
			result.result = "(RegPush) DB error.";
			res.status(500).json(result);
			return;
		}

		if (at) {
			at.pushID = pushID;
			at.save(function(err, product, numAff) {
				if (err) {// DB Error
					console.log("Save pushID error, user: " + at.loginID);
					result.result = "(RegPush) save PushID error.";
					res.status(500).json(result);
					return;
				}
				// Success
				console.log("User " + at.loginID + " registered pushID.");
				result.result = "User " + at.loginID + " registered pushID.";
				res.status(200).json(result);
			});
		}
	});
};

exports.send = function(req, loginID, message, payload) {
	req.db.AccessToken.find({
		loginID : loginID
	}, function(err, accessTokens) {
		if (err) { // DB Error
			return err;
		}
		try{
			if (accessTokens) {
				for(var i in accessTokens){
					if(accessTokens[i].pushID){
						var accessToken = accessTokens[i];
						var pushID = accessToken.pushID;
						if (pushID && pushID.trim().length > 0) {
							console.log("sending notification to: " + loginID + " pushID: " + pushID);
							if (accessToken.deviceType === "iOS" || accessToken.deviceType === "iPad") {
								apns.init();
								apns.send({token: pushID, message: message, from: "Ahitis", payload: payload});
								var a = "a";
							} else if (accessToken.deviceType === "Android") {
								gcms.send({token: pushID, message: message, from: "Ahitis", payload: payload});
							}
						}
					}
				}
			}
		}catch(err){
			throw err;
		}
	});
};
