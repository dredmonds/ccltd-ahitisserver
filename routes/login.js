/*****************************************************************
* Module Name: Login router
* Description: Login router of Ahitis server
* Author: Brian Lai
* Date Modified: 2016.01.04
* Related files: n/a
*****************************************************************/
//Require the md5 module.
var md5 = require('md5');
var nodemailer = require('nodemailer');

//The purpose of this login router is to obtain the user from DB and check if the password match.
//If match, generate an accesstoken for the user. and send back to the user.
exports.auth = function(req, res) {
	//Grab object send by client device.
	var username = req.body.username;
	var password = req.body.password;
	var deviceUUID = req.body.deviceUUID;
	var deviceType = req.body.deviceType;

	//If no deviceUUID send by client, it should be from Web Browser.
	if(!deviceUUID){
		deviceUUID = "web";
		deviceType = "web";
	}
	//Prepare the response object.
	var result = {
		result: "",
		accessToken: "",
		userType: ""
	};

	console.log("User " + username + " trying to login...");

	//Search for the user in Database
	req.db.User.findOne({loginID: username}, function(err, user){
		if(err){ //DB error
			result.result = "Login error.";
			res.status(500).json(result);
			return;
		}

		if(user){ //User found.
			//Check if password match.
			if (user.password === password) {
				result.result = "Login succses.";

				req.db.AccessToken.remove({deviceUUID: deviceUUID, loginID: username}, function(err){
					if(err){ //DB error
						result.result = "Remove deviceUUID error.";
						res.status(500).json(result);
						return;
					}
					//Success
				});

				//Prepare the db object
				var accessToken = new req.db.AccessToken({
					deviceUUID: deviceUUID,
					accessToken: md5(deviceUUID + new Date().getTime()),
					deviceType: deviceType,
					loginID: req.body.username,
					pushID: " ",
					deviceToken: "1",
					createdDate: new Date()
				});

				//prepare the response value. AccessToken and userType to send back to the user.
				result.accessToken = accessToken.accessToken;
				result.userType = user.userType;
				console.log("firstname: " + user.firstname + " | lastname: " + user.lastname);
				result.firstname = user.firstname;
				result.lastname = user.lastname;

				//Save the access token to DB
				accessToken.save(function(err, product, numAff) {
					if (err) { //DB error
						console.log("Create key error");
						result.result = "Create key error.";
						res.status(500).json(result);
						return;
					}
					//Success
					console.log("Created access token for user " + username);

					res.status(200).json(result);
					return;
				});
			} else { //Password not match.
				result.result = "Username or Password not match.";
				res.status(403).json(result);
				return;
			}
		}else{ //User not found
			result.result = "Username or Password not match.";
			res.status(403).json(result);
			return;
		}
	});
};

exports.forgotPassword = function(req, res){
	var loginID = req.body.username;
	var result = {
			result: ""
	};

	req.db.User.findOne({loginID: loginID}, function(err, user){
		if(err){
			result.result = "forgot password error.";
			res.status(500).json(result);
			return;
		}

		if(user){
			user.verificationCode = md5(user.loginID + new Date().getTime()).substring(0,4);
			user.save(function(err, product, numAff){

				// create reusable transporter object using the default SMTP transport
				var transporter = nodemailer.createTransport('smtps://admin%40ahitis.com:DijudAm8=@web044.dnchosting.com');

				// setup e-mail data with unicode symbols
				var mailOptions = {
				    from: '"AHitis Admin" <admin@ahitis.com>', // sender address
				    to: user.email, // list of receivers
				    subject: 'Hi ' + user.firstname + ' ' + user.lastname + ', you triggered a reset password. ', // Subject line
				    html: '<html><head><meta content="text/html;charset=UTF-8" http-equiv="content-type" /><title></title></head><body><p>Please use the below verification Code and input it into reset password page</p><br/>Verification Code: <b>' + user.verificationCode + '</b></body></html>'// plaintext body
				};

				// send mail with defined transport object
				transporter.sendMail(mailOptions, function(error, info){
				    if(error){
				        console.log(error);
						result.result = "Email send error.";
						res.status(500).json(result);
						return;
				    }else{
				    	console.log('Message sent: ' + info.response);

						//Success
						result.result = "Verification email sent.";
						res.status(200).json(result);
						return;
				    }
				});

			});
		}
	});
}

exports.resetPassword = function(req,res){
	var verificationCode = req.body.verificationCode;
	var password = req.body.password;
	var result = {
			result: ""
	};

	req.db.User.findOne({verificationCode: verificationCode}, function(err, user){
		if(err){
			result.result = "reset password error.";
			res.status(500).json(result);
			return;
		}

		if(user){
			user.password = password;
			user.save(function(err, product, numAff){
				if(err){
					result.result = "reset password error.";
					res.status(500).json(result);
					return;
				}

				result.result = "Password has been reset.";
				res.status(200).json(result);
				return;
			});
		}else{
			result.result = "Wrong verification code.";
			res.status(403).json(result);
			return;
		}
	})
}

exports.activation = function(req,res){
	var activationKey = req.body.activationKey;
	var result = {
		result: "",
		userType: ""
	};

	console.log("ActivationKey: " + activationKey);
	req.db.User.findOne({activationKey: activationKey}, function(err, user){
		if(err){
			result.result = "Activation Error";
			res.status(500).json(result);
			return;
		}

		if(user){
			user.activationStatus = 1;
			user.save(function(err, product, numAff){
				result.result = "Your AHitis account have been activated.";
				result.userType = product.userType;
				res.status(200).json(result);
				return;
			});
		}else{
			result.result = "Wrong activation key.";
			res.status(403).json(result);
			return;
		}
	});
}

exports.logout = function(req,res){
	var loginID = req.loginID;
	var result = {
		result: ""
	};

	console.log("User " + loginID + " trying to logout...");

	//Search for the user in Database
	req.db.User.findOne({loginID: loginID}, function(err, user){
		if(err){ //DB error
			result.result = "Logout error.";
			res.status(500).json(result);
			return;
		}

		if(user){ //User found.
			if(user.userType === "Worker"){
				//Parallel free worker
				req.db.OnlineWorker.findOne({loginID: loginID}, function(err,worker){
					if(err){ //DB error
						result.result = "Offline Worker error.";
						res.status(500).json(result);
						return;
					}

					if(worker){
						if(worker.jobID == null || worker.jobID.length == 0){
							//Remove the onlineworker from DB using the loginID.
							req.db.OnlineWorker.remove({loginID: loginID}, function(err){
								if(err){ //DB error
									result.result = "Offline Worker error.";
									res.status(500).json(result);
									return;
								}

								req.db.AccessToken.remove({loginID: loginID}, function(err){
									if(err){ //DB error
										result.result = "Logout error.";
										res.status(500).json(result);
										return;
									}
									//Success
									result.result = "Logout success.";
									res.status(200).json(result);
									return;
								});
							});
						}else{
							result.result = "Cannot logout, You are currently working on a job.";
							res.status(406).json(result);
							return;
						}
					}else{
						req.db.AccessToken.remove({loginID: loginID}, function(err){
							if(err){ //DB error
								result.result = "Logout error.";
								res.status(500).json(result);
								return;
							}
							//Success
							result.result = "Logout success.";
							res.status(200).json(result);
							return;
						});
					}
				});
			}else{
				req.db.AccessToken.remove({loginID: loginID}, function(err){
					if(err){ //DB error
						result.result = "Logout error.";
						res.status(500).json(result);
						return;
					}
					//Success
					result.result = "Logout success.";
					res.status(200).json(result);
					return;
				});
			}
		}else{
			result.result = "Logout error.";
			res.status(500).json(result);
			return;
		}
	});
}
