/*****************************************************************
* Module Name: User information router
* Description: User information router of Ahitis server
* Author:
* Date Modified: 2016.01.19
* Related files: n/a
*****************************************************************/
var https = require('https');
exports.getUser = function(req, res){
	var userID = req.body.userID;
	//Prepare for response object
	var result = {
		result: "",
		skill: {}
	};
	//DB query to User to get user's information
	req.db.User.findOne({loginID: userID}, function(err, user) {
		if(err){ //DB Error
			result.result = "DB error.";
			res.status(500).json(result);
			return;
		}

		if(user){
			if(user.loginID === "") {
				result.result = "User's profile DB query are empty.";
				res.status(403).json(result);
				return;
			} else {
					result.loginID = user.loginID;
					result.password = user.password;  /*insert this to re-used for updating user profile*/
					result.firstname = user.firstname;
					result.lastname = user.lastname;
					result.email = user.email;
					result.contactNo = user.contactNo;
					result.userType = user.userType;
					result.countryCode = user.countryCode;
					result.skill.skillID = user.skill.skillID;
					result.skill.ranking = user.skill.ranking;
					result.skill.duration = user.skill.duration;
					result.skill.amount = user.skill.amount;
					result.profilePict = user.profilePict;
					result.createdDate = user.createdDate;
					result.currentDate = new Date();
					console.log("User's profile DB query success.");
					res.status(200).json(result);
					return;
			 }
		}else{
			result.result = "User's profile DB query are empty.";
			res.status(403).json(result);
			return;
		}
	});
}

// Get user's profile information
exports.profile = function(req, res){
	//Prepare for response object
	var result = {
		result: "",
		skill: {}
	};
	//DB query to User to get user's information
	req.db.User.findOne({loginID: req.loginID}, function(err, user) {
		if(err){ //DB Error
			result.result = "DB error.";
			res.status(500).json(result);
			return;
		}

		if(user){
			if(user.loginID === "") {
				result.result = "User's profile DB query are empty.";
				res.status(403).json(result);
				return;
			} else {
					result.loginID = user.loginID;
					result.password = user.password;  /*insert this to re-used for updating user profile*/
					result.firstname = user.firstname;
					result.lastname = user.lastname;
					result.email = user.email;
					result.contactNo = user.contactNo;
					result.userType = user.userType;
					result.countryCode = user.countryCode;
					result.skill.skillID = user.skill.skillID;
					result.skill.ranking = user.skill.ranking;
					result.skill.duration = user.skill.duration;
					result.skill.amount = user.skill.amount;
					result.activationStatus = user.activationStatus;
					result.createdDate = user.createdDate;
					result.profilePict = user.profilePict;
					console.log("User's profile DB query success.");
					res.status(200).json(result);
					return;
			}
		}else{
			result.result = "User's profile DB query are empty.";
			res.status(403).json(result);
			return;
		}
	});

};


//Update user's profile information
exports.update = function(req, res){
	//Grab object send by client device.
	var username = req.body.username;                     /*No rules yet, if included for updates*/
	var password = req.body.password;
	var firstname = req.body.firstname;
	var lastname = req.body.lastname;
	var email = req.body.email;
	var contactNo = req.body.contactNo;
	//var userType = req.body.userType;                   /*No rules yet, if included for updates*/
	var countryCode = req.body.countryCode;
	var profilePict = req.body.profilePict;  /*inserted to update user's profile picture*/
	console.log(profilePict);

	//Prepare the response object.
	var result = {
		result : ""
	};

	console.log("User " + username + " trying to updates information...");

	//Prepare the DB object
	var updtUser = {
			//logID: username,                            /*No rules yet, if included for updates*/
			password : password,
			firstname : firstname,
			lastname : lastname,
			email : email,
			contactNo : contactNo,
			//userType: userType,                         /*No rules yet, if included for updates*/
			countryCode: countryCode,
			updatedBy: req.logID,
			updatedDate: new Date(),
			profilePict: profilePict                      /*inserted to update user's profile picture*/
			};

	//Update User DB
	req.db.User.update({loginID: req.loginID},{$set: updtUser}, {multi:true}, function(err, product, numAff) {
		if (err) {//DB Error
			console.log("Error updating user " + username);
			result.result = "Update error.";
			res.status(500).json(result);
			return;
		}
		//Success
		result.result = "User " + username + " updated.";
		res.status(200).json(result);
	});/*end of DB update*/

};


//Authenticate user password for u_profile_edit
exports.authPassword = function(req, res){
	//Get the loginID obtained in the middleware using the access token.
	var loginID = req.loginID;
	//Grab object send by client device.
	var password = req.body.password;
	var result = {
			result : ""
		};

	console.log("User password " + loginID + " trying to verify...");

	//Search for the user in Database
	req.db.User.findOne({loginID: loginID}, function(err, user){
		if(err){ //DB error
			result.result = "500";
			res.status(500).json(result);
			return;
		}

		if(user){
			if(user.password === password){
				result.result = "200";
				res.status(200).json(result);
				return;
			}else{ //Password not match.
				result.result = "403";
				res.status(403).json(result);
				return;
			}

		}else{ //User not found
			result.result = "403";
			res.status(403).json(result);
			return;
		}

	});
};

exports.countryCode = function(req,res){
	var loginID = req.loginID;
	var long = req.body.long;
	var lat = req.body.lat;
	var countryCode = "";
	//Prepare the response object.
	var result = {
		result : ""
	};

	//this option object is for Google API use. (Pass the coordinate to get the address)
	var optionsget= {
			host: 'maps.googleapis.com',
			port: 443,
			path: '/maps/api/geocode/json?latlng=' + lat +',' + long + '&result_type=country&key=' + req.googleKey,
			method: 'GET'
	};

	console.info('Calling Google Map GeoLocation API for country code');
	// do the google GET request
	var location = "";
	https.get(optionsget, function(googleRes) {
			console.log("statusCode: ", googleRes.statusCode);

			//The http get request handle the response from google in stream mode.
			//Therefore, the on 'data' event may not contains all the data. Google may send the information in multiple packet.
			//the on 'data' event here to append the characters into location variable.
			//The on 'end' event handle the location variable to obtain the country code.
			googleRes.on('data', function(d) {
				location += d.toString('utf8');
			}).on('end', function(){
			var locationObj = JSON.parse(location); //Parse the string into JSON object.
				if(locationObj.status === "OK"){ //If status from google say OK
					for(var i in locationObj.results){ //loop through the results
						if(locationObj.results[i].types[0] === "country"){ //Checks if the location type is country.
							countryCode = locationObj.results[i].address_components[0].short_name; //get the short_name from address_commponents. This is the ISO country Code.
						}
					}
				}

				result.countryCode = countryCode;
				res.status(200).json(result);

			}).on('error', function(e) {
				console.error(e);
				result.result = "Get countryCode error.";
				res.status(500).json(result);
				return;
			});
		});

};
