/*****************************************************************
* Module Name: Price information router
* Description: Price information router of Ahitis server
* Author:
* Date Modified: 2016.09.20
* Related files: n/a
*****************************************************************/
exports.listPrice = function(req, res){
	//Prepare for response object
	var result = {
		result: "",
		prices: []
	};
	//DB query to User to get user's information
	req.db.Price.find({}, function(err, prices) {
		if(err){ //DB Error
			result.result = "DB error.";
			res.status(500).json(result);
			return;
		}

		if(prices){
			console.log("Prices count: " + prices.length);
      prices.forEach(function(price){
        var priceObj = {};
        priceObj.skillID = price.skillid;
        priceObj.price = [];
        price.price.forEach(function(countryPrice){
          var countryPriceObj = {};
          countryPriceObj.country = countryPrice.country;
          countryPriceObj.currencyLbl = countryPrice.currencyLbl;
          countryPriceObj.currency = countryPrice.currency;
          countryPriceObj.pricePerUnit = countryPrice.pricePerUnit;
          priceObj.price.push(countryPriceObj);
        });
        result.prices.push(priceObj);
      });
      console.log("prices DB query success.");
			res.status(200).json(result);
			return;
		}else{
			result.result = "No Prices Found";
			console.log("No prices in DB.");
			res.status(500).json(result);
			return;
		}

  });

}
