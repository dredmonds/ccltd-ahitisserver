/*****************************************************************
* Module Name: Main router
* Description: Main router of Ahitis server
* Author: Brian Lai
* Date Modified: 2016.01.04
* Related files: n/a
*****************************************************************/
// Module dependencies.
var express = require('express');
var mongoose = require('mongoose');
var models = require('./models/models');
//Add all routes
var routes = require('./routes');
var user = require('./routes/user');
var login = require('./routes/login');
var auth = require('./routes/auth');
var register = require('./routes/register');
var worker = require('./routes/worker');
var skill = require('./routes/skill');
var price = require('./routes/price');
var job = require('./routes/job');
var notification = require('./routes/notification');
var uploads = require('./routes/uploads');
var md5 = require('md5');
var http = require('http');
var https = require('https');
var fs = require('fs');
var path = require('path');
var cors = require('cors');
var util = require('util');
var favicon = require('serve-favicon');
var logger = require('morgan');
var multer = require('multer');
var methodOverride = require('method-override');
var session = require('express-session');
var bodyParser = require('body-parser');
var errorHandler = require('errorhandler');
var payment = require('./routes/payment');     /*inserted by dredmonds for payment 2016.02.03*/
var braintree = require('braintree');          /*inserted by dredmonds for payment 2016.09.07*/
var dispute = require('./routes/dispute'); /*HR 2016.06.30*/
var errorrep = require('./routes/errorrep'); /*HR 2016.07.06*/
var app = express();

var log_file = fs.createWriteStream(__dirname + '/debug.log', {flags : 'w'});
var log_stdout = process.stdout;

console.log = function(d) { //
	  log_file.write(new Date().toISOString().replace(/T/, ' ').replace(/\..+/, '') + ' | ' +  util.format(d) + '\n');
	  log_stdout.write(new Date().toISOString().replace(/T/, ' ').replace(/\..+/, '') + ' | ' +  util.format(d) + '\n');
	};

//all environments
app.set('httpPort', process.env.PORT || 3000);
app.set('httpsPort', process.env.PORT || 3333);
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');
//DB path, need to change to read property file
app.set('dbURL', 'localhost:27017');
app.use(favicon(__dirname + '/public/favicon.ico'));
app.use(logger('dev'));
app.use(methodOverride());
app.use(session({ resave: true,
                  saveUninitialized: true,
                  secret: 't3st12' }));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(express.static(path.join(__dirname, 'public')));
app.use(cors());

var options = {
   key  : fs.readFileSync('hub.ahitis.com.key'),
   cert : fs.readFileSync('1_hub.ahitis.com_bundle.crt')
};

//Create DB connection and test if it connected.
var connection = mongoose.createConnection('mongodb://' + app.get('dbURL') + "/ahitis");
connection.on('error', console.error.bind(console, 'DB connection error: '));
connection.once('open', function() {
	console.log('Connected to DB');
});

// Add db models into req object
function db(req, res, next) {
	req.db = {
		User : connection.model('User', models.User, 'users'),
		AccessToken : connection.model('AccessToken', models.AccessToken,
				'accesstokens'),
		OnlineWorker: connection.model('OnlineWorker',models.OnlineWorker,'onlineWorkers'),
		Skill: connection.model('Skill',models.Skill,'skills'),
		Price: connection.model('Price',models.Price,'prices'),
    Dispute: connection.model('Dispute',models.Dispute,'dispute'),
		ErrorRep: connection.model('ErrorRep',models.ErrorRep,'errorrep'),
		Language: connection.model('Language',models.Language,'languages'),
		Job: connection.model('Job',models.Job,'jobs'),
		Status: connection.model('Status',models.Status,'statuses'),
		PaymentStatus: connection.model('PaymentStatus',models.PaymentStatus,'paymentStatuses'),
		Transaction: connection.model('Transaction',models.Transaction,'transactions')
	};
	req.googleKey = "AIzaSyDBTyoa0LLIaeHW0XWJDwLi_xmqJqzycgQ";

	req.baseURL = "http://hub.ahitis.com:3000";        //Inserted by dredmonds to redirect at mobile page 2016.05.19

	//Inserted by dredmonds to configure Payment API credentials 2016.09.07
	req.gateway = braintree.connect({
		/*Braintree Sandbox API Keys*/

        environment:  braintree.Environment.Sandbox,
        merchantId:   'hfzmzkhyvz8vfp2f',
        publicKey:    'mkqwfx7wfqzn5ys5',
        privateKey:   '18f3e69888697af3544e8d08a353818d'


		/*Braintree Production API Keys*/
		/*
        environment:  braintree.Environment.Production,
        merchantId:   'pydz43s8qqgzqt54',
        publicKey:    'g5xd2ypck7pktttn',
        privateKey:   '8f50e5aed265036fb544c23377e40bec'
		*/
	});

	return next();
}

// check client token
app.use(function(req,res,next){

	if(req.path === '/login' || req.path === '/auth' || req.path === '/register'
		|| req.path === '/payment/paypalreturn' || req.path === '/payment/paypalcancel' || req.path === '/paymentTX'
		|| req.path.lastIndexOf('/uploads/depositCard', 0) === 0
		|| req.path.lastIndexOf('/activation', 0) === 0 || req.path.lastIndexOf('/uploads/profilePict', 0) === 0
		|| req.path === '/forgotPassword' || req.path === '/resetPassword'
    || req.path === '/activation' )
	return next();

	//Get the accessToken from payload or query
	var accessToken = req.body.accessToken || req.query.accessToken;
	console.log(req.ip + " validating access token.");

	var result = {
			result : ""
		};
	//If accessToken send from client device.
	if (accessToken) {
		//Because this middleware run before the function db, so the req object do not contain the models.
		req.db = {
			User : connection.model('User', models.User, 'users'),
			AccessToken : connection.model('AccessToken', models.AccessToken,
					'accesstokens')
		};
		//find the accessToken entry from DB
		req.db.AccessToken.findOne({
			accessToken : accessToken
		}, function(err, at) {
			//if DB error
			if (err) {
				result.result = "Check key error.";
				res.status(500).json(result);
				return;
			}

			//Set the expire date to be 7 days after the access token creation date
			var expire = new Date();
			expire.setDate(expire.getDate() - 7);

			//If accessToken entry found
			if (at) {
				//Check the token if expired
				if (Math.floor(expire - at.createdDate) <= 0) { //not expired
					result.result = "Valid access token.";
					at.createdDate = new Date();
					//Update the access token entry with new date.
					at.save(function(err, product, numAff) {
						if (err) { //DB error
							console.log("Update key error");
							result.result = "Update key error.";
							res.status(500).json(result);
							return;
						}
						//Success, store the loginID and deviceType into req object for next function use.
						req.loginID = at.loginID;
						req.deviceType = at.deviceType;
						return next();
					});
				} else { //expired
					result.result = "Access token expired.";
					res.status(419).json(result);
					return;
				}
			} else { //If accessToken not found
				result.result = "Invalid access token.";
				res.status(401).json(result);
				return;
			}
		});
	}else{ //no access token
		result.result = "Access Token required";
		res.status(403).json(result);
	}
});

// development only
if ('development' === app.get('env')) {
	app.use(errorHandler());
}

var storageDepositCard = multer.diskStorage({
	  destination: function (req, file, cb) {
	    cb(null, './uploads/depositCard')
	  },
	  filename: function (req, file, cb) {
	    cb(null, req.body.loginID + '-' + req.body.fileName)
	  }
	});

var uploadDepositCard = multer({ storage: storageDepositCard });

//Inserted by dredmonds 13/06/2016
var storageProfilePict = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, './uploads/profilePict')
  },
  filename: function (req, file, cb) {
    cb(null, req.body.loginID + '-' + req.body.fileName)
  }
});

var uploadProfilePict = multer({ storage: storageProfilePict });

//Routes. The first parameter: path to match. parameter after: functions to run in sequence.
app.get('/', cors(), routes.index);
app.post('/login', cors(), db, login.auth);
app.get('/logout', cors(), db, login.logout); //inserted by brian 25/04/2016
app.post('/forgotPassword', cors(), db, login.forgotPassword); //inserted by brian 17/06/2016
app.post('/resetPassword', cors(), db, login.resetPassword); //inserted by brian 17/06/2016
app.post('/auth', cors(), db, auth.auth);
app.post('/register', cors(), db, register.user);
app.post('/register/depositCard', cors(), db, register.depositCard);
app.post('/register/updateDepositCard', cors(), db, register.updateDepositCard); //inserted by dredmonds 08/07/2016
app.get('/register/getDepositCard', cors(), db, register.getDepositCard); //inserted by dredmonds 08/07/2016
app.post('/activation',cors(),db, login.activation);
app.post('/onlineWorker', cors(), db, worker.onlineWorker);
app.post('/onlineWorker/jobReject', cors(), db, worker.jobReject);
app.post('/offlineWorker', cors(), db, worker.offlineWorker);
app.get('/skill/name', cors(), db, skill.listName); //inserted by brian 07/04/2016
app.post('/skill/skillID', cors(), db, skill.skillID); //inserted by brian 07/04/2016
app.get('/price', cors(), db, price.listPrice); //inserted by Brian 18/09/2016
app.get('/user', cors(), db, user.profile); //inserted by dredmonds 11/01/2016
app.post('/user', cors(), db, user.update); //inserted by dredmonds 15/01/2016
app.post('/user/getUser', cors(), db, user.getUser); //inserted by brian 21/06/2016
app.post('/user/countryCode', cors(), db, user.countryCode); //inserted by brian 22/09/2016
app.get('/worker', cors(), db, worker.profile); //inserted by dredmonds 19/01/2016
app.post('/worker', cors(), db, worker.update); //inserted by dredmonds 19/01/2016
app.post('/worker/getWorker', cors(), db, worker.getWorker); //inserted by brian 21/06/2016
app.post('/worker/search', cors(), db, worker.search); //inserted by brian 19/04/2016
app.get('/job/userList', cors(), db, job.userList); //inserted by brian 20/04/2016
app.get('/job/workerList', cors(), db, job.workerList); //inserted by brian 20/04/2016
app.post('/job/jobDetail', cors(), db, job.jobID); //inserted by brian 01/05/2016
app.post('/job/jobComplete', cors(), db, job.jobComplete); //inserted by brian 07/06/2016
app.post('/job/jobPaymentDetail', cors(), db, job.jobPaymentDetail); //inserted by brian 07/06/2016
app.post('/job/jobAccept', cors(), db, job.jobAccept); //inserted by brian 01/05/2016
app.post('/job/jobReject', cors(), db, job.jobReject); //inserted by brian 01/05/2016
app.post('/regPush', cors(), db, notification.reg); //inserted by brian 20/01/2016
app.post('/uploads/depositCard', cors(), db, uploadDepositCard.single('depositCard'), uploads.depositCard); //inserted by brian 20/01/2016
app.get('/uploads/depositCard/:name', uploads.getImage);
app.post('/uploads/profilePict', cors(), db, uploadProfilePict.single('profilePict'), uploads.saveProfilePict); //inserted by dredmonds 15/06/2016
app.get('/uploads/profilePict/:name', uploads.getProfilePict); //inserted by dredmonds 15/06/2016
app.post('/usrAuthPassword', cors(), db, user.authPassword); //inserted by dredmonds 01/02/2016
app.post('/wrkAuthPassword', cors(), db, worker.authPassword); //inserted by dredmonds 01/02/2016
app.get('/paymentPaypal', cors(), db, payment.getPaypalCredentials); //inserted by dredmonds 19/04/2016
app.get('/payment/paypalreturn', cors(), db, payment.urlPaypalreturn); //inserted by dredmonds 13/05/2016
app.get('/payment/paypalcancel', cors(), db, payment.urlPaypalcancel); //inserted by dredmonds 13/05/2016
app.post('/dispute', cors(), db, dispute.registerDispute); /*HR 2016.06.30*/
app.post('/errorrep', cors(), db, errorrep.registerErrorRep); /*HR 2016.06.30*/
app.get('/paymentToken', cors(), db, payment.getToken); //inserted by dredmonds 08/09/2016
app.post('/paymentTX', cors(), db, payment.postTransaction); //inserted by dredmonds 08/09/2016

//start the server.
var httpsServer = https.createServer(options, app);
httpsServer.listen(
		app.get('httpsPort'),
		function() {
			console.log('Express server listening on '	+ httpsServer.address().address + 'port ' + httpsServer.address().port);
    });
var httpServer = http.createServer(app);
httpServer.listen(
    app.get('httpPort'),
    function() {
      console.log('Express server listening on '	+ httpServer.address().address + 'port ' + httpServer.address().port);
    });
