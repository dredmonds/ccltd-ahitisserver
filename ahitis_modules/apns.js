/*******************************************************************************
 * Module Name: Apple Push Notification Service (APNS)
 * Description: Send push notification to client device
 * Author: Brian Lai Date Modified: 2016.01.20.
 * Related files: /keys
 ******************************************************************************/
var apn  = require("apn")

var apnError = function(err){
    console.log("APN Error:", err);
}

/*
var options = {
    cert: "dev_apn_cert.pem",
    key: "dev_apn_key.pem",
    passphrase: "asdf",
    production: false,
    port: 2195,
    enhanced: true,
    cacheLength: 5
  };
*/
var options = {
    "cert": "pro_apn_cert.pem",
    "key": "pro_apn_key.pem",
    "ca": ["entrust_2048_ca.cer"],
    "passphrase": "ccAcc0unt",
    "production": true,
    "port": 2195
  };
options.errorCallback = apnError;

var feedBackOptions = {
  "cert": "pro_apn_cert.pem",
  "key": "pro_apn_key.pem",
  "ca": ["entrust_2048_ca.cer"],
  "passphrase": "ccAcc0unt",
  "production": true,
  "port": 2195,
  "batchFeedback": true,
  "interval": 300
};

var apnConnection, feedback;

module.exports = {
    init : function(){
        apnConnection = new apn.Connection(options);

        feedback = new apn.Feedback(feedBackOptions);
        feedback.on("feedback", function(devices) {
            devices.forEach(function(item) {
                //TODO Do something with item.device and item.time;
            });
        });
    },

    send : function (params){
        var myDevice, note;

        console.log(params.token);
        try{
            myDevice = new apn.Device(params.token);
            note = new apn.Notification();

            note.expiry = Math.floor(Date.now() / 1000) + 3600; // Expires 1 hour from now.
            note.badge = 0;
            note.sound = "ping.aiff";
            note.alert = params.message;
            note.payload = {'messageFrom': params.from, 'payload': params.payload};

            if(apnConnection) {
                apnConnection.pushNotification(note, myDevice);
            }
        }catch(err){
        	console.log(err.message);
        	throw err;
        }
    }
}

/*usage
pushNotifier = require("/ahitis_modules/apns");
pushNotifier.init();
//use valid device token to get it working
pushNotifier.send({token:'', message:'Test message', from: 'sender'});
*/
