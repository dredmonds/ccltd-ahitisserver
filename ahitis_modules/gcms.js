/*******************************************************************************
 * Module Name: Google Cloud Messaging Service (GCMS)
 * Description: Send push notification to client device
 * Author: Brian Lai Date Modified: 2016.01.20.
 * Related files: /keys
 ******************************************************************************/
var gcm = require('node-gcm');
var message = new gcm.Message();

//API Server Key
var sender = new gcm.Sender('AIzaSyDBTyoa0LLIaeHW0XWJDwLi_xmqJqzycgQ');
var registrationIds = [];

module.exports = {
    send : function (params){
    	registrationIds = [params.token];
    	// Value the payload data to send...
    	var message = new gcm.Message();
    		//priority: 'high',
    		//contentAvailable: true,
    		//delayWhileIdle: true,
    		//timeToLive: 3000,
    		//dryRun: false,
    		//data: {
    		//	payload: params.payload,
        //  message: params.message,
        //  title: params.from,
    		//	icon: "ic_launcher",
    		//	body: params.message,
        //  visibility: 1,
        //  priority: 2,
        //  delayWhileIdle: true,
        //  timeToLive: 3000,
        //  dryRun: false
    		//}
//    		notification: {
//    			title: params.from,
//    			icon: "ic_launcher",
//    			body: params.message
//    		}
    	//});
      //message.addData('content-available', '1');
    	message.addData('message',params.message);
    	message.addData('title', params.from);
    	message.addData('icon', 'ic_launcher'); 
    	message.addData('delayWhileIdle', '1');
      message.addData('timeToLive','3000');
      message.addData('dryRun','false');
      message.addData('payload',params.payload);
      message.addData('visibility','1');
      message.addData('priority','2');

      //message.addData('body',params.message);

    	//message.addData('msgcnt','3'); // Shows up in the notification in the status bar
    	//message.addData('soundname','beep.wav'); //Sound to play upon notification receipt - put in the www folder in app
    	//message.timeToLive = 3000;// Duration in seconds to hold in GCM and retry before timing out. Default 4 weeks (2,419,200 seconds) if not specified.

    	/**
    	 * Parameters: message-literal, registrationIds-array, No. of retries, callback-function
    	 */
    	sender.send(message, {registrationTokens: registrationIds}, function (err, response) {
    		if(err) console.error(err);
    		else 	console.log(response);
    	});
    }
}
