/*****************************************************************
* Module Name: Mongoose Models
* Description: Mongoose Models of Ahitis server
* Author: Brian Lai
* Date Modified: 2016.01.04
* Related files: n/a
*****************************************************************/
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

//Models for mongoose to communicate to DB and as an object to play in Code.
exports.User = new Schema({
	loginID: { type:String, required: true, trim: true},
	password: { type:String, required: true, trim: true},
	firstname: { type:String, required: true, trim: true},
	lastname: { type:String, required: true, trim: true},
	email: { type:String, required: true, trim: true},
	contactNo: { type:String, trim: true},
	userType: { type:String, required: true, trim: true},
	countryCode: { type:String, trim: true},
	skill: {
		skillID: { type:Number, trim: true},
		ranking: { type:Number, trim: true},
		duration: { type:Number, trim: true},
		amount: {type:Number}
	},
	card: {
		DBSref: { type:String, trim: true},
		name: { type:String, trim: true},
		address: { type:String, trim: true},
		city: { type:String, trim: true},
		postCode: { type:String, trim: true},
		day: { type:String, trim: true},
		month: { type:String, trim: true},
		year: { type:String, trim: true},
		bankName: { type:String, trim: true},
		accountNumber: { type:String, trim: true},
		sortCode: { type:String, trim: true},
		IBAN: { type:String, trim: true},
		swiftCode: { type:String, trim: true}
	},
	activationKey: {type:String, trim:true},
	activationStatus: {type:Number},
	verificationCode: {type:String, trim:true},
	profilePict: { type:String, trim: true},
	createdBy: { type:String, required: true, trim: true},
	createdDate: { type:Date, required: true},
	updatedBy: String,
	updatedDate: Date
});

exports.Dispute = new Schema({
	disputeID: { type: Schema.Types.ObjectId, auto: true},
	jobID: { type: Schema.Types.ObjectId, required: true, trim: true},
	reason: {type: String, required: true, trim: true},
	createdBy: { type:String, required: true, trim: true},
	createdDate: { type:Date, required: true},
	updatedBy: String,
	updatedDate: Date
});

exports.ErrorRep = new Schema({
	errorRepID: { type: Schema.Types.ObjectId, auto: true},
	errorRep: {type: String, required: true, trim: true},
	createdBy: { type:String, required: true, trim: true},
	createdDate: { type:Date, required: true},
	updatedBy: String,
	updatedDate: Date
});

exports.Skill = new Schema({
	skillid: { type:Number, required: true, trim: true},
	name: { type:String, required: true, trim: true},
	description: String,
	icon: {type: String, trim: true},
	ranking: { type:Number, required: true},
	unit: { type:Number, required: true},
	sandboxID: {type:String, trim:true},
	liveID: {type:String, trim:true},
	createdBy: { type:String, required: true, trim: true},
	createdDate: { type:Date, required: true},
	updatedBy: String,
	updatedDate: Date
});

exports.Price = new Schema({
	skillid: { type:Number, required: true, trim: true},
	price: [{
			country: String,
			currencyLbl: String,
			currency: String,
			pricePerUnit: { type:Number, required: true}
	}],
	createdBy: { type:String, required: true, trim: true},
	createdDate: { type:Date, required: true},
	updatedBy: String,
	updatedDate: Date
});

exports.Language = new Schema({
	languageID: { type:Number, required: true, trim: true},
	name: { type:String, required: true, trim: true},
	createdBy: { type:String, required: true, trim: true},
	createdDate: { type:Date, required: true},
	updatedBy: String,
	updatedDate: Date
});

exports.Job = new Schema({
	jobID: { type: Schema.Types.ObjectId, auto: true},
	userID: { type:String, required: true, trim: true},
	workerID: { type:String, required: false, trim: true},
	skillID: { type:Number, required: true, trim: true},
	duration: {type:Number},
	amount: {type:Number},
	loc: {type: [Number], index: '2dsphere', required: true},
	countryCode: { type:String, required: false, trim: true},
	statusID: { type:Number, required: true, trim: true},
	paymentStatusID: { type:Number, required: true, trim: true},
	createdBy: { type:String, required: true, trim: true},
	createdDate: { type:Date, required: true},
	updatedBy: String,
	updatedDate: Date
});

exports.Status = new Schema({
	statusID: { type:Number, required: true, trim: true},
	name: { type:String, required: true, trim: true},
	description: String,
	createdBy: { type:String, required: true, trim: true},
	createdDate: { type:Date, required: true},
	updatedBy: String,
	updatedDate: Date
});

exports.PaymentStatus = new Schema({
	paymentStatusID: { type:Number, required: true, trim: true},
	name: { type:String, required: true, trim: true},
	description: String,
	createdBy: { type:String, required: true, trim: true},
	createdDate: { type:Date, required: true},
	updatedBy: String,
	updatedDate: Date
});

exports.AccessToken = new Schema({
	deviceUUID: {type: String, required: true, trim: true},
	accessToken: {type: String, required: true, trim: true},
	loginID: {type: String, required: true, trim: true},
	pushID: String,
	deviceType: {type: String, required: true, trim: true},
	deviceToken: {type: String, required: true, trim: true},
	createdDate: { type:Date, required: true}
});

exports.OnlineWorker = new Schema({
	loginID:{type: String, required: true, trim: true},
	skillID:{type: Number, required: true, trim: true},
	loc: {type: [Number], index: '2dsphere', required: true},
	countryCode: {type: String, required: true, trim: true},
	jobID: { type: Schema.Types.ObjectId, auto: true},
	onlineDateTime: {type:Date, required: true}
});

exports.Transaction = new Schema({
	transactionID:{type: String, required: true, trim: true},
	jobID: { type:String, required: true, trim: true},
	amount: {type: Number, required: true},
	currency: {type: String, required: true, trim: true},
	paymentDate: { type:Date, required: true},
	paymentStatus: { type:String, required: true, trim: true},
	createdBy: { type:String, required: true, trim: true},
	createdDate: { type:Date, required: true},
	updatedBy: String,
	updatedDate: Date
});
